ALTER TABLE "admin-web"."public"."web_user" DROP COLUMN IF EXISTS update_by;

ALTER TABLE "admin-web"."public"."web_role" DROP COLUMN IF EXISTS role_id;

ALTER TABLE "admin-web"."public"."web_role" DROP COLUMN IF EXISTS web_user_id;