INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (56, 1, '标签管理', 'web/cms/tag', NULL, 1, 'tag', 2);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (57, 56, '查看', NULL, 'sys:tag:select', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (58, 56, '新增', NULL, 'sys:tag:save', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (59, 56, '修改', NULL, 'sys:tag:update', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (60, 56, '删除', NULL, 'sys:tag:delete', 2, NULL, 0);