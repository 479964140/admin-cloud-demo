drop table if exists web_role_menu;
drop table if exists web_role;
drop table if exists web_menu;
drop table if exists web_user_token;
drop table if exists web_user;
/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     2019/6/20 11:06:04                           */
/*==============================================================*/




/*==============================================================*/
/* Table: web_menu                                              */
/*==============================================================*/
create table web_menu (
   id                   VARCHAR(32)          not null,
   parent_id            VARCHAR(32)          null,
   name                 VARCHAR(32)          null,
   url                  VARCHAR(255)         null,
   perms                VARCHAR(255)         null,
   type                 INT2                 null,
   icon                 VARCHAR(255)         null,
   sort                 VARCHAR(32)          null,
   create_by            VARCHAR(32)          null,
   operate_by           VARCHAR(32)          null,
   create_time          TIMESTAMP            null,
   update_time          TIMESTAMP            null,
   deleted              INT2                 null,
   constraint PK_WEB_MENU primary key (id)
);

comment on table web_menu is
'菜单表';

comment on column web_menu.id is
'主键';

comment on column web_menu.parent_id is
'父级id';

comment on column web_menu.name is
'名称';

comment on column web_menu.url is
'接口';

comment on column web_menu.perms is
'权限';

comment on column web_menu.type is
'权限类型';

comment on column web_menu.icon is
'图标';

comment on column web_menu.sort is
'排序';

comment on column web_menu.create_by is
'创建人';

comment on column web_menu.operate_by is
'修改人';

comment on column web_menu.create_time is
'创建时间';

comment on column web_menu.update_time is
'修改时间';

comment on column web_menu.deleted is
'是否删除';

/*==============================================================*/
/* Table: web_role                                              */
/*==============================================================*/
create table web_role (
   id                   VARCHAR(32)          not null,
   name                 VARCHAR(32)          null,
   create_by            VARCHAR(32)          null,
   operate_by           VARCHAR(32)          null,
   create_time          TIMESTAMP            null,
   update_time          TIMESTAMP            null,
   deleted              INT2                 null,
   constraint PK_WEB_ROLE primary key (id)
);

comment on table web_role is
'角色表';

comment on column web_role.id is
'主键';

comment on column web_role.name is
'角色';

comment on column web_role.create_by is
'创建人';

comment on column web_role.operate_by is
'修改人';

comment on column web_role.create_time is
'创建时间';

comment on column web_role.update_time is
'修改时间';

comment on column web_role.deleted is
'是否删除';

/*==============================================================*/
/* Table: web_role_menu                                         */
/*==============================================================*/
create table web_role_menu (
   role_id              VARCHAR(32)          null,
   menu_id              VARCHAR(32)          null
);

comment on table web_role_menu is
'角色菜单表';

comment on column web_role_menu.role_id is
'主键';

comment on column web_role_menu.menu_id is
'主键';

/*==============================================================*/
/* Table: web_user                                              */
/*==============================================================*/
create table web_user (
   id                   VARCHAR(32)          not null,
   username             VARCHAR(32)          null,
   password             VARCHAR(32)          null,
   salt                 VARCHAR(32)          null,
   email                VARCHAR(32)          null,
   phone                VARCHAR(11)          null,
   create_by            VARCHAR(32)          null,
   create_time          TIMESTAMP            null,
   operate_by           VARCHAR(32)          null,
   update_time          TIMESTAMP            null,
   role_id              VARCHAR(32)          null,
   state                INT2                 null,
   deleted              INT2                 null,
   constraint PK_WEB_USER primary key (id)
);

comment on table web_user is
'用户';

comment on column web_user.id is
'主键';

comment on column web_user.username is
'用户名';

comment on column web_user.password is
'创建人';

comment on column web_user.salt is
'盐值';

comment on column web_user.email is
'邮件';

comment on column web_user.phone is
'修改人';

comment on column web_user.create_by is
'是否删除';

comment on column web_user.create_time is
'创建时间';

comment on column web_user.operate_by is
'修改人';

comment on column web_user.update_time is
'修改时间';

comment on column web_user.role_id is
'角色';

comment on column web_user.state is
'状态';

comment on column web_user.deleted is
'是否删除';

/*==============================================================*/
/* Table: web_user_token                                        */
/*==============================================================*/
create table web_user_token (
   id                   VARCHAR(32)          not null,
   user_id              VARCHAR(32)          null,
   token                VARCHAR(255)         null,
   expire_time          TIMESTAMP            null,
   update_time          TIMESTAMP            null,
   constraint PK_WEB_USER_TOKEN primary key (id)
);

comment on table web_user_token is
'用户token';

comment on column web_user_token.id is
'id';

comment on column web_user_token.user_id is
'用户id';

comment on column web_user_token.token is
'token';

comment on column web_user_token.expire_time is
'销毁时间';

comment on column web_user_token.update_time is
'更新时间';

