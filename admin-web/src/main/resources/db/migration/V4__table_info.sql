INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (31, 1, '类目管理', 'web/cms/items', NULL, 1, 'items', 2);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (32, 31, '查看', NULL, 'sys:items:select', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (33, 31, '新增', NULL, 'sys:items:save', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (34, 31, '修改', NULL, 'sys:items:update', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (35, 31, '删除', NULL, 'sys:items:delete', 2, NULL, 0);

INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (36, 1, '内容管理', 'web/cms/content', NULL, 1, 'content', 2);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (37, 36, '查看', NULL, 'sys:content:select', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (38, 36, '新增', NULL, 'sys:content:save', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (39, 36, '修改', NULL, 'sys:content:update', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (40, 36, '删除', NULL, 'sys:content:delete', 2, NULL, 0);

INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (41, 1, '用户管理', 'web/user', NULL, 1, 'user', 2);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (42, 41, '查看', NULL, 'sys:user:select', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (43, 41, '新增', NULL, 'sys:user:save', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (44, 41, '修改', NULL, 'sys:user:update', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (45, 41, '删除', NULL, 'sys:user:delete', 2, NULL, 0);

INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (46, 1, '用户反馈管理', 'web/userService/suggestion', NULL, 1, 'suggestion', 2);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (47, 46, '查看', NULL, 'sys:suggestion:select', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (48, 46, '新增', NULL, 'sys:suggestion:save', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (49, 46, '修改', NULL, 'sys:suggestion:update', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (50, 46, '删除', NULL, 'sys:suggestion:delete', 2, NULL, 0);

INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (51, 1, '活动管理', 'web/cms/activity', NULL, 1, 'activity', 2);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (52, 51, '查看', NULL, 'sys:activity:select', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (53, 51, '新增', NULL, 'sys:activity:save', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (54, 51, '修改', NULL, 'sys:activity:update', 2, NULL, 0);
INSERT INTO web_menu(id, parent_id, name, url, perms, type, icon, sort) VALUES (55, 51, '删除', NULL, 'sys:activity:delete', 2, NULL, 0);