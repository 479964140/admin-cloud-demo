package com.chensi.admin.web.domain;

import com.chensi.admin.web.common.Constants;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

/**
 * @date 2019/6/12 9:00
 */

@Data
@Entity(name = "cms_tag")
@Where(clause = "deleted = 0")
public class CmsTag extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "id required", groups = Constants.Update.class)
    private String id;

    @NotBlank(message = "标签名不能为空")
    private String name;

    private String sort;

    private Integer state;
}
