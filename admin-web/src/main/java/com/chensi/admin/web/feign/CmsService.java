package com.chensi.admin.web.feign;

import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.common.JsonResult;
import com.chensi.admin.web.domain.CmsActivity;
import com.chensi.admin.web.domain.CmsContent;
import com.chensi.admin.web.domain.CmsItems;
import com.chensi.admin.web.domain.CmsTag;
import com.chensi.admin.web.dto.CmsActivityDTO;
import com.chensi.admin.web.dto.CmsContentDTO;
import com.chensi.admin.web.dto.CmsItemsDTO;
import com.chensi.admin.web.dto.CmsTagDTO;
import com.chensi.admin.web.exception.BaseException;
import com.chensi.admin.web.feign.factory.CmsFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Date: 2019/6/24 9:45
 */

@FeignClient(value = "CMS", path = Constants.CMS_PATH, fallbackFactory = CmsFallbackFactory.class)
public interface CmsService {

    //----------------------------ξ类目接口ξ--------------------------*/

    /**
     * CMS-类目-查询所有类目
     *
     * @return 类目集合
     */
    @GetMapping(value = "/items")
    ResponseEntity<JsonResult<List<CmsItemsDTO>>> listItems() throws BaseException;

    /**
     * CMS-类目-查询类目详情
     *
     * @param id 类目主键id
     * @return 类目详情
     * @throws BaseException 查询异常
     */
    @GetMapping("/items/{id}")
    ResponseEntity<JsonResult> getItems(@PathVariable("id") String id) throws BaseException;

    /**
     * CMS-类目-新增类目
     *
     * @param cmsItems 类目信息
     * @return 新增状态
     * @throws BaseException 新增异常
     */
    @PostMapping("/items")
    ResponseEntity<JsonResult> saveItems(@Valid @RequestBody CmsItems cmsItems) throws BaseException;

    /**
     * CMS-类目-删除类目
     *
     * @param id 类目主键id
     * @return 删除状态
     * @throws BaseException 删除异常
     */
    @DeleteMapping("/items/{id}")
    ResponseEntity<JsonResult> deleteItems(@PathVariable("id") String id) throws BaseException;

    /**
     * CMS-类目-更新类目
     *
     * @param id       主键id
     * @param cmsItems 类目信息
     * @return 更新状态
     * @throws BaseException 更新异常
     */
    @PutMapping("/items/{id}")
    ResponseEntity<JsonResult> updateItems(
            @PathVariable("id") String id,
            @Validated(Constants.Update.class) @RequestBody CmsItems cmsItems)
            throws BaseException;

    /**
     * CMS-类目-分页查询类目
     *
     * @param pageNum  当前页
     * @param pageSize 每页几条
     * @param name     类目条件查询
     * @return 分页集合
     */
    @RequestMapping(value = "/items/page", method = RequestMethod.GET)
    ResponseEntity<JsonResult> pageItems(
            @RequestParam(value = "pageNum", required = false) Integer pageNum,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "name", required = false) String name) throws BaseException;

    //------------------------------ξ内容接口ξ-----------------------------*/

    /**
     * CMS-内容-删除内容
     *
     * @param id 内容id
     * @return 删除状态
     * @throws BaseException 删除异常
     */
    @DeleteMapping("/content/{id}")
    ResponseEntity<JsonResult> deleteContent(@PathVariable("id") String id)
            throws BaseException;

    /**
     * CMS-内容-更新内容
     *
     * @param id         主键id
     * @param cmsContent 内容信息
     * @return 更新状态
     * @throws BaseException 更新异常
     */
    @PutMapping("/content/{id}")
    ResponseEntity<JsonResult> updateContent(@PathVariable("id") String id,
                                             @Validated(Constants.Update.class) @RequestBody CmsContent cmsContent)
            throws BaseException;

    /**
     * CMS-内容-获取内容详情
     *
     * @param id 主键id
     * @return 内容详情
     * @throws BaseException 查询异常
     */
    @GetMapping("/content/{id}")
    ResponseEntity<JsonResult> getContent(@PathVariable("id") String id) throws BaseException;

    /**
     * CMS-内容-查询内容集合
     *
     * @return 内容集合
     */
    @GetMapping("/content")
    ResponseEntity<JsonResult<List<CmsContentDTO>>> listContent() throws BaseException;

    /**
     * CMS-内容-更新内容
     *
     * @param cmsContent 内容信息
     * @return 更新状态
     * @throws BaseException 更新异常
     */
    @PostMapping("/content")
    ResponseEntity<JsonResult> saveContent(
            @Valid @RequestBody CmsContent cmsContent) throws BaseException;

    /**
     * CMS-咨询内容-分页查询内容
     *
     * @param cmsItemsId 条件查询-类目id
     * @param pageNum    当前页
     * @param pageSize   分页长度
     * @param title      内容标题
     * @return 分页查询、条件查询咨询内容
     */
    @RequestMapping(value = "/content/page", method = RequestMethod.GET)
    ResponseEntity<JsonResult> pageContent(
            @RequestParam(value = "cmsItemsId", required = false) String cmsItemsId,
            @RequestParam(value = "pageNum", required = false) Integer pageNum,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "title", required = false) String title) throws BaseException;

    //------------------------------ξ标签接口ξ-------------------------*/

    /**
     * CMS-标签-删除标签
     *
     * @param id 主键id
     * @return 删除状态
     * @throws BaseException 删除异常
     */
    @DeleteMapping("/tag/{id}")
    ResponseEntity<JsonResult> deleteTag(@PathVariable("id") @NotNull String id)
            throws BaseException;

    /**
     * CMS-标签-更新标签
     *
     * @param id     主键id
     * @param cmsTag 标签信息
     * @return 更新状态
     * @throws BaseException 更新异常
     */
    @PutMapping("/tag/{id}")
    ResponseEntity<JsonResult> updateTag(
            @PathVariable("id") String id,
            @Validated(Constants.Update.class) @RequestBody CmsTag cmsTag)
            throws BaseException;

    /**
     * CMS-标签-查询标签详情
     *
     * @param id 主键id
     * @return 标签详情
     * @throws BaseException 查询异常
     */
    @GetMapping("/tag/{id}")
    ResponseEntity<JsonResult> getTag(@PathVariable("id") String id) throws BaseException;

    /**
     * CMS-标签-查询标签集合
     *
     * @return 标签集合
     */
    @GetMapping("/tag")
    ResponseEntity<JsonResult<List<CmsTagDTO>>> listTag() throws BaseException;

    /**
     * CMS-标签-更新标签
     *
     * @param cmsTag 标签信息
     * @return 更新状态
     * @throws BaseException 更新异常
     */
    @PostMapping("/tag")
    ResponseEntity<JsonResult> saveTag(
            @Valid @RequestBody CmsTag cmsTag) throws BaseException;

    /**
     * CMS-标签-分页查询
     *
     * @param pageNum  当前页
     * @param pageSize 每页条数
     * @param name     标签名称
     * @return 分页查询集合、条件查询集合
     */
    @RequestMapping(value = "/tag/page", method = RequestMethod.GET)
    ResponseEntity<JsonResult> pageTag(
            @RequestParam(value = "pageNum", required = false) Integer pageNum,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "name", required = false) String name) throws BaseException;

    //------------------------------ξ活动接口ξ-------------------------*/

    /**
     * CMS-活动-删除活动
     *
     * @param id 活动id
     * @return 删除状态
     * @throws BaseException 删除异常
     */
    @DeleteMapping("/activity/{id}")
    ResponseEntity<JsonResult> deleteActivity(@PathVariable("id") String id)
            throws BaseException;

    /**
     * CMS-活动-更新活动
     *
     * @param id          主键id
     * @param cmsActivity 活动信息
     * @return 更新状态
     * @throws BaseException 更新异常
     */
    @PutMapping("/activity/{id}")
    ResponseEntity<JsonResult> updateActivity(
            @PathVariable("id") String id,
            @Validated(Constants.Update.class) @RequestBody CmsActivity cmsActivity)
            throws BaseException;

    /**
     * CMS-活动-获取活动详情
     *
     * @param id 主键id
     * @return 活动详情
     * @throws BaseException 查询异常
     */
    @GetMapping("/activity/{id}")
    ResponseEntity<JsonResult> getActivity(@PathVariable("id") String id) throws BaseException;

    /**
     * CMS-活动-查询活动集合
     *
     * @return 活动集合
     */
    @GetMapping("/activity")
    ResponseEntity<JsonResult<List<CmsActivityDTO>>> listActivity() throws BaseException;

    /**
     * CMS-活动-更新活动
     *
     * @param cmsActivity 活动信息
     * @return 更新状态
     * @throws BaseException 更新异常
     */
    @PostMapping("/activity")
    ResponseEntity<JsonResult> saveActivity(
            @Valid @RequestBody CmsActivity cmsActivity) throws BaseException;

    /**
     * CMS-活动-分页活动
     *
     * @param pageNum  当前页
     * @param pageSize 分页长度
     * @param title    活动标题
     * @return 分页查询、条件查询活动
     */
    @RequestMapping(value = "/activity/page", method = RequestMethod.GET)
    ResponseEntity<JsonResult> pageActivity(
            @RequestParam(value = "pageNum", required = false) Integer pageNum,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "title", required = false) String title) throws BaseException;

}
