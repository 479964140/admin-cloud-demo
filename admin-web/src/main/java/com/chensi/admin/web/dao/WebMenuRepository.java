package com.chensi.admin.web.dao;

import com.chensi.admin.web.domain.WebMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @date 2019/6/20 14:10
 */

@Repository
public interface WebMenuRepository extends JpaRepository<WebMenu, String>, JpaSpecificationExecutor<WebMenu> {

    Optional<WebMenu> findByNameAndParentIdAndDeleted(String name, String parentId, Integer deleted);

    /**
     * 删除菜单时删除中间表中menu_id
     *
     * @param menuId 菜单id
     */
    @Modifying
    @Query(value = "DELETE FROM web_role_menu WHERE menu_id = ?1 ", nativeQuery = true)
    void delMenuId(String menuId);

    /**
     * 查询菜单在中间表中是否被角色使用
     *
     * @param menuId 菜单id
     * @return
     */
    @Query(value = "SELECT DISTINCT menu_id FROM web_role_menu WHERE menu_id = ?1 ", nativeQuery = true)
    String queryMenuId(String menuId);
}
