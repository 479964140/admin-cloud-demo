package com.chensi.admin.web.oauth2;

import com.chensi.admin.web.exception.BaseException;
import com.chensi.admin.web.exception.UnknownException;

import java.security.MessageDigest;
import java.util.UUID;

/**
 * @Author: si.chen
 * @Date: 2019/6/19 11:26
 */
public class TokenGenerator {


    public static String generateValue() throws BaseException {
        return generateValue(UUID.randomUUID().toString());
    }

    private static final char[] HEX_CODE = "0123456789abcdef".toCharArray();

    private static String toHexString(byte[] data) {
        if (data == null) {
            return null;
        }
        StringBuilder r = new StringBuilder(data.length * 2);
        for (byte b : data) {
            r.append(HEX_CODE[(b >> 4) & 0xF]);
            r.append(HEX_CODE[(b & 0xF)]);
        }
        return r.toString();
    }

    private static String generateValue(String param) throws BaseException {
        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(param.getBytes());
            byte[] messageDigest = algorithm.digest();
            return toHexString(messageDigest);
        } catch (Exception e) {
            throw new UnknownException(e);
        }
    }

}
