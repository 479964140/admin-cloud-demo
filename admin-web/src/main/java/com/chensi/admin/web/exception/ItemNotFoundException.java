package com.chensi.admin.web.exception;


import com.chensi.admin.web.common.BaseErrorCode;

public class ItemNotFoundException extends BaseException {

    public ItemNotFoundException(String description) {
        super(BaseErrorCode.ITEM_NOT_FOUND.getCode(), BaseErrorCode.ITEM_NOT_FOUND.getMessage(), description);
    }
}
