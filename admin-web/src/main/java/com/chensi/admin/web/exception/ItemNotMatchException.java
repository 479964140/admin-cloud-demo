package com.chensi.admin.web.exception;


import com.chensi.admin.web.common.BaseErrorCode;

public class ItemNotMatchException extends BaseException {

    public ItemNotMatchException(String description) {
        super(BaseErrorCode.ITEM_NOT_MATCH.getCode(), BaseErrorCode.ITEM_NOT_MATCH.getMessage(), description);
    }
}
