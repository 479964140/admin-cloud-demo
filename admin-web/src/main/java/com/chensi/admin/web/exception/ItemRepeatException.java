package com.chensi.admin.web.exception;


import com.chensi.admin.web.common.BaseErrorCode;

public class ItemRepeatException extends BaseException {
    public ItemRepeatException(String description) {
        super(BaseErrorCode.ITEM_REPEAT.getCode(), BaseErrorCode.ITEM_REPEAT.getMessage(), description);
    }
}
