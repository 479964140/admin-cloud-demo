package com.chensi.admin.web.service;

import com.chensi.admin.web.domain.WebMenu;
import com.chensi.admin.web.dto.WebMenuDTO;
import com.chensi.admin.web.exception.BaseException;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @date 2019/6/20 14:05
 */
public interface WebMenuService {

    List<WebMenuDTO> list();

    WebMenuDTO get(String id) throws BaseException;

    Page<WebMenuDTO> page(String name, Integer pageNum, Integer pageSize);

    WebMenuDTO create(WebMenu webMenu) throws BaseException;

    void update(WebMenu webMenu) throws BaseException;

    void delete(String id) throws BaseException;
}
