package com.chensi.admin.web.domain;

import com.chensi.admin.web.common.Constants;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @date 2019/6/20 10:48
 */

@Getter
@Setter
@Entity(name = "web_role")
@Where(clause = "deleted = 0")
public class WebRole extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 7169576893405923733L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "id required", groups = Constants.Update.class)
    private String id;

    @NotBlank(message = "角色名称不能为空!")
    private String name;

    @ManyToMany
    @JoinTable(name = "web_role_menu",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
            , inverseJoinColumns = @JoinColumn(name = "menu_id", referencedColumnName = "id"))
    private Set<WebMenu> webMenus = new HashSet<>();


}
