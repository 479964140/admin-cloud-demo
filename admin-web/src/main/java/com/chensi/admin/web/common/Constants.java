package com.chensi.admin.web.common;

/**
 * @author si.chen
 * @date 2019/6/20 14:09
 */
public class Constants {
    /**
     * 删除标志： 否
     */
    public static final int DELETED_NO = 0;
    /**
     * 删除标志： 是
     */
    public static final int DELETED_YES = 1;

    public static final Integer PAGE_NUM = 0;
    public static final Integer PAGE_SIZE = 10;

    /**
     * 超级管理员用户名
     */
    public static final String SUPER_ADMIN_USERNAME = "admin";

    /**
     * 超级管理员用户名角色id
     */
    public static final String ROLE_ID = "4028df816baccf6a016bacdf87ac0000";

    /**
     * 超级管理员ID
     */
    public static final String SUPER_ADMIN = "1";


    /**
     * 登录失败次数上限
     */
    public static final Integer LOGIN_ERROR_LIMIT = 5;

    /**
     * 登录失败时间范围（秒）：单位时间内的登录失败
     */
    public static final Integer LOGIN_ERROR_TIME_SCOPE = 30 * 60;

    /**
     * Token失效时间
     */
    public static final Integer TOKEN_EXPIRE = 30 * 60;

    /**
     * 用户状态：正常：0
     */
    public static final Integer USER_STATE_NORMAL = 0;

    /**
     * 用户状态：锁定：1
     */
    public static final Integer USER_STATE_LOCKED = 1;

    /**
     * http状态
     */
    public static final Integer HTTP_CODE = 200;
    public static final String CODE = "0";


    public static final String CMS_PATH = "/cms";

    public static final String USERSERVICE_PATH = "/user";

    public static final String EDUCATION_PATH = "/edu";

    /**
     * hibernate validator groups
     */
    public interface Create {
    }

    public interface Update {
    }

    public interface Login {
    }

    public interface Password {
    }

    public interface CheckId {
    }
}