package com.chensi.admin.web.dto;

import com.chensi.admin.web.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @date 2019/6/179:49
 */
@Data
@ApiModel("反馈内容")
public class UserSuggestionDTO implements Serializable {

    private static final long serialVersionUID = -7349069482260866313L;

    @ApiModelProperty(value = "id", required = true)
    private String id;

    @ApiModelProperty(value = "内容标题-title", required = true)
    private String content;

    @ApiModelProperty(value = "创建人", required = true)
    private String createBy;

    @ApiModelProperty(value = "处理人", required = true)
    private String operateBy;

    @ApiModelProperty(value = "处理状态")
    private Integer state;

    @ApiModelProperty(value = "是否删除状态")
    private Integer deleted;


    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime operateTime;


}
