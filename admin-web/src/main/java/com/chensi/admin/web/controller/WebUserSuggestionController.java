package com.chensi.admin.web.controller;

import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.domain.UserSuggestion;
import com.chensi.admin.web.dto.search.SearchSuggestionDTO;
import com.chensi.admin.web.exception.BaseException;
import com.chensi.admin.web.feign.UserSuggestionService;
import com.chensi.admin.web.util.JsonUtil;
import com.chensi.admin.web.common.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @date 2019/7/215:43
 */

@RestController
@RequestMapping("/web/userService")
public class WebUserSuggestionController {

    private final UserSuggestionService userSuggestionService;

    @Autowired
    @SuppressWarnings("all")
    public WebUserSuggestionController(UserSuggestionService userSuggestionService) {
        this.userSuggestionService = userSuggestionService;
    }

    /**
     * 保存意见反馈
     *
     * @param userSuggestion 意见反馈
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 逻辑异常
     */
    @PostMapping("/suggestion")
    ResponseEntity<JsonResult> save(@Valid @RequestBody UserSuggestion userSuggestion) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = userSuggestionService.save(userSuggestion);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    /**
     * 删除意见反馈
     *
     * @param id 意见反馈id
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 逻辑异常
     */
    @DeleteMapping("/suggestion/{id}")
    ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = userSuggestionService.delete(id);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    /**
     * 修改用户反馈
     *
     * @param id             用户反馈id
     * @param userSuggestion 用户反馈
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 逻辑异常
     */
    @PutMapping("/suggestion/{id}")
    ResponseEntity<JsonResult> update(@PathVariable("id") String id, @Validated({Constants.Update.class})
    @RequestBody UserSuggestion userSuggestion) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = userSuggestionService.update(id, userSuggestion);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    /**
     * 分页查询用户反馈
     *
     * @param searchSuggestionDTO 分页类
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 逻辑异常
     */
    @GetMapping("/suggestion/page")
    ResponseEntity<JsonResult> page(SearchSuggestionDTO searchSuggestionDTO)
            throws BaseException {
        ResponseEntity<JsonResult> jsonResult =
                userSuggestionService.page(searchSuggestionDTO.getPageNum(), searchSuggestionDTO.getPageSize());
        return JsonUtil.isRightOrNot(jsonResult);
    }


}
