package com.chensi.admin.web.domain;

import com.chensi.admin.web.common.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @date 2019/7/2215:18
 */

@Getter
@Setter
@Entity(name = "edu_school")
@EntityListeners(AuditingEntityListener.class)
public class EduSchool extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -754083815896036356L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    @ApiModelProperty(value = "ID")
    private String id;

    @NotBlank(message = "注册编码不为空")
    @ApiModelProperty(value = "注册编码")
    @Column(name = "code")
    private String code;

    @NotBlank(message = "名称不为空")
    @ApiModelProperty(value = "名称")
    @Column(name = "name")
    private String name;

    @NotBlank(message = "简介不为空")
    @ApiModelProperty(value = "简介")
    @Column(name = "introduction")
    private String introduction;

    @NotBlank(message = "学校性质不为空")
    @ApiModelProperty(value = "学校性质")
    @Column(name = "nature")
    private String nature;

    @ApiModelProperty(value = "状态")
    @Column(name = "state")
    private Integer state = Constants.DELETED_NO;

    @NotBlank(message = "联系人不为空")
    @ApiModelProperty(value = "联系人")
    @Column(name = "contact")
    private String contact;

    @NotBlank(message = "法律人不为空")
    @ApiModelProperty(value = "法律人")
    @Column(name = "leader")
    private String leader;

    @NotBlank(message = "联系电话不为空")
    @ApiModelProperty(value = "联系电话")
    @Column(name = "phone")
    private String phone;

    @NotBlank(message = "地址不为空")
    @ApiModelProperty(value = "地址")
    @Column(name = "address")
    private String address;

    @NotBlank(message = "成立时间不为空")
    @ApiModelProperty(value = "成立时间")
    @Column(name = "establish")
    private String establish;

    @ApiModelProperty(value = "备注")
    @Column(name = "note")
    private String note;

}
