package com.chensi.admin.web.util;

import org.apache.commons.collections4.CollectionUtils;

import java.util.Set;

public class CollectionUtil {
    public static String DEFUALT_SPLITTER = ",";


    public static String toString(Set<String> sets, String splitter) {
        if (CollectionUtils.isEmpty(sets)) return "";
        return sets.stream().reduce((s1, s2) -> s1 + splitter + s2).orElse("");
    }

    public static String toString(Set<String> sets) {
        return toString(sets, DEFUALT_SPLITTER);
    }
}
