package com.chensi.admin.web.dto.search;

import lombok.Data;

/**
 * @Date: 2019/6/10 17:38
 */
@Data
public class SearchPageDTO {

    private Integer pageNum;

    private Integer pageSize;
}
