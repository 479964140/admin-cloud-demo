package com.chensi.admin.web.feign;
import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.common.JsonResult;
import com.chensi.admin.web.domain.EduSchool;
import com.chensi.admin.web.exception.BaseException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @date 2019/7/2313:17
 */
@FeignClient(value = "EDUCATION", path = Constants.EDUCATION_PATH)
public interface EduSchoolService {

    /**
     * 创建学校
     * @param eduSchool eduSchool
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 基本异常
     */
    @PostMapping("/school")
    ResponseEntity<JsonResult> create(@Valid @RequestBody EduSchool eduSchool) throws BaseException;

    /**
     * 删除学校
     * @return ResponseEntity<JsonResult>
     * @param id id
     * @throws BaseException 基本异常
     */
    @DeleteMapping("/school/{id}")
    ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException;

    /**
     * 更新学校信息
     * @param id id
     * @param eduSchool eduSchool
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 基本异常
     */
    @PutMapping("/school/{id}")
    ResponseEntity<JsonResult> update(@PathVariable("id") String id, @Validated({Constants.Update.class})
    @RequestBody EduSchool eduSchool) throws BaseException;

    /**
     * 获取学校信息列表
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @param name name
     * @return ResponseEntity<JsonResult>
     * @throws BaseException  基本异常
     */
    @GetMapping("/school/page")
    ResponseEntity<JsonResult> page(
            @RequestParam(value = "pageNum", required = false) Integer pageNum,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "name", required = false) String name
    )
            throws BaseException;

    /**
     * 获取学校信息
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 基本异常
     */
    @GetMapping("/school")
    ResponseEntity<JsonResult> list() throws BaseException;
}
