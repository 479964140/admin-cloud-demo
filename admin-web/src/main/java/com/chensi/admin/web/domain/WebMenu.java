package com.chensi.admin.web.domain;

import com.chensi.admin.web.common.Constants;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @date 2019/6/20 10:53
 */

@Data
@Entity(name = "web_menu")
@Where(clause = "deleted = 0")
public class WebMenu extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -5711348265326931983L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "id required", groups = Constants.Update.class)
    private String id;

    @Column(name = "parent_id")
    private String parentId;

    @NotBlank(message = "菜单名称不能为空!")
    private String name;

    private String url;

    private String perms;

    private Integer type;

    private String icon;

    private String sort;

}
