package com.chensi.admin.web.controller;

import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.common.JsonResult;
import com.chensi.admin.web.controller.api.WebMenuApi;
import com.chensi.admin.web.domain.WebMenu;
import com.chensi.admin.web.dto.WebMenuDTO;
import com.chensi.admin.web.dto.search.SearchWebMenuDTO;
import com.chensi.admin.web.exception.BaseException;
import com.chensi.admin.web.service.WebMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @date 2019/6/20 17:55
 */

@RestController
public class WebMenuController implements WebMenuApi {

    private final WebMenuService webMenuService;

    @Autowired
    public WebMenuController(WebMenuService webMenuService) {
        this.webMenuService = webMenuService;
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<JsonResult> delete(
            @NotBlank(message = "id不能为空") @PathVariable("id") String id) throws BaseException {
        webMenuService.delete(id);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity<JsonResult> update(
            @NotBlank(message = "id不能为空") @PathVariable("id") String id,
            @Validated(Constants.Update.class) @RequestBody WebMenu webMenu) throws BaseException {
        webMenuService.update(webMenu);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<JsonResult> get(
            @NotBlank(message = "id不能为空") @PathVariable("id") String id) throws BaseException {
        return ResponseEntity.ok(JsonResult.ok(webMenuService.get(id)));
    }

    @Override
    public ResponseEntity<JsonResult<List<WebMenuDTO>>> list() {
        return ResponseEntity.ok(
                JsonResult.ok(webMenuService.list())
        );
    }

    @Override
    public ResponseEntity<JsonResult<Page<WebMenuDTO>>> page(SearchWebMenuDTO searchWebMenuDTO) {
        return ResponseEntity.ok(
                JsonResult.ok(
                        webMenuService.page(
                                searchWebMenuDTO.getName()
                                , searchWebMenuDTO.getPageNum()
                                , searchWebMenuDTO.getPageSize()
                        )
                )
        );
    }

    @Override
    public ResponseEntity<JsonResult> save(@Valid @RequestBody WebMenu webMenu) throws BaseException {
        return ResponseEntity.ok(JsonResult.ok(webMenuService.create(webMenu)));
    }
}
