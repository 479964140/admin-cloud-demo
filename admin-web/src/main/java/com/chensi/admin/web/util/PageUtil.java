package com.chensi.admin.web.util;

import com.chensi.admin.web.common.Constants;

import java.util.Optional;

/**
 * @date 2019/6/20 14:58
 */
public class PageUtil {

    private static final Integer ZERO = 0;

    private static final Integer POSITIVE = 1;

    public static Integer pageNum(Integer pageNum) {
        return Optional.ofNullable(pageNum).filter(n -> n >= ZERO).orElse(Constants.PAGE_NUM);
    }

    public static Integer pageSize(Integer pageSize) {
        return Optional.ofNullable(pageSize).filter(n -> n > POSITIVE).orElse(Constants.PAGE_SIZE);
    }
}
