package com.chensi.admin.web.dto;

import com.chensi.admin.web.domain.WebRole;
import com.chensi.admin.web.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @date 2019/6/1915:49
 */

@Data
@ApiModel("用户内容")
public class WebUserDTO {


    @ApiModelProperty(value = "主键", required = true)
    private String id;

    @ApiModelProperty(value = "用户名", required = true)
    private String username;

    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @ApiModelProperty(value = "创建人", required = true)
    private String createBy;

    @ApiModelProperty(value = "修改人", required = true)
    private String operateBy;

    @ApiModelProperty(value = "权限信息", required = true)
    private WebRole webRole;

    @ApiModelProperty(value = "邮件", required = true)
    private String email;

    @ApiModelProperty(value = "电话", required = true)
    private String phone;

    @ApiModelProperty(value = "状态")
    private Integer state;

    @ApiModelProperty(value = "是否删除状态")
    private Integer deleted;


    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime updateTime;
}
