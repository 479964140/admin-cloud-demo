package com.chensi.admin.web.domain;

import com.chensi.admin.web.common.Constants;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

/**
 * @Date: 2019/6/10 17:10
 */

@Getter
@Setter
@Entity(name = "cms_items")
@Where(clause = "deleted = 0")
public class CmsItems extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "id required", groups = Constants.Update.class)
    private String id;

    @NotBlank(message = "类目不能为空")
    private String name;

    private String sort;
}
