package com.chensi.admin.web.feign.fallback;

import com.chensi.admin.web.feign.CmsService;
import com.chensi.admin.web.common.BaseErrorCode;
import com.chensi.admin.web.common.JsonResult;
import com.chensi.admin.web.domain.CmsActivity;
import com.chensi.admin.web.domain.CmsContent;
import com.chensi.admin.web.domain.CmsItems;
import com.chensi.admin.web.domain.CmsTag;
import com.chensi.admin.web.dto.CmsActivityDTO;
import com.chensi.admin.web.dto.CmsContentDTO;
import com.chensi.admin.web.dto.CmsItemsDTO;
import com.chensi.admin.web.dto.CmsTagDTO;
import com.chensi.admin.web.exception.BaseException;
import lombok.Setter;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/29 10:39
 */
@Component
public class CmsFallbackImpl implements CmsService {

    @Setter
    private Throwable cause;

    @Override
    public ResponseEntity<JsonResult<List<CmsItemsDTO>>> listItems() throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> getItems(String id) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> saveItems(@Valid CmsItems cmsItems) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> deleteItems(String id) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> updateItems(String id, CmsItems cmsItems) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());

    }

    @Override
    public ResponseEntity<JsonResult> pageItems(Integer pageNum, Integer pageSize, String name) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> deleteContent(String id) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> updateContent(String id, CmsContent cmsContent) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> getContent(String id) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult<List<CmsContentDTO>>> listContent() throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> saveContent(@Valid CmsContent cmsContent) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> pageContent(String cmsItemsId, Integer pageNum, Integer pageSize, String title) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> deleteTag(@NotNull String id) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> updateTag(String id, CmsTag cmsTag) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> getTag(String id) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult<List<CmsTagDTO>>> listTag() throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> saveTag(@Valid CmsTag cmsTag) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> pageTag(Integer pageNum, Integer pageSize, String name) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> deleteActivity(String id) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> updateActivity(String id, CmsActivity cmsActivity) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> getActivity(String id) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult<List<CmsActivityDTO>>> listActivity() throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> saveActivity(@Valid CmsActivity cmsActivity) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> pageActivity(Integer pageNum, Integer pageSize, String title) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }
}
