package com.chensi.admin.web.feign;

import com.chensi.admin.web.feign.factory.UserSuggestionFallbackFactory;
import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.common.JsonResult;
import com.chensi.admin.web.domain.UserSuggestion;
import com.chensi.admin.web.exception.BaseException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @date 2019/7/215:30
 */
@FeignClient(value = "USERSERVICE", path = Constants.USERSERVICE_PATH,
        fallbackFactory = UserSuggestionFallbackFactory.class)
public interface UserSuggestionService {

    /**
     * 保存意见反馈
     *
     * @param userSuggestion 意见反馈
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 逻辑异常
     */
    @PostMapping("/suggestion")
    ResponseEntity<JsonResult> save(@Valid @RequestBody UserSuggestion userSuggestion) throws BaseException;

    /**
     * 删除意见反馈
     *
     * @param id 意见反馈id
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 逻辑异常
     */
    @DeleteMapping("/suggestion/{id}")
    ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException;

    /**
     * 修改用户反馈
     *
     * @param id             用户反馈id
     * @param userSuggestion 用户反馈
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 逻辑异常
     */
    @PutMapping("/suggestion/{id}")
    ResponseEntity<JsonResult> update(@PathVariable("id") String id, @Validated({Constants.Update.class})
    @RequestBody UserSuggestion userSuggestion) throws BaseException;

    /**
     * 分页查询用户反馈
     *
     * @param pageNum  分页类
     * @param pageSize 分页类
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 逻辑异常
     */
    @GetMapping("/suggestion/page")
    ResponseEntity<JsonResult> page(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize)
            throws BaseException;
}
