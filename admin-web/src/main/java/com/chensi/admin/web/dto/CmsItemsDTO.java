package com.chensi.admin.web.dto;

import com.chensi.admin.web.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Date: 2019/6/10 17:15
 */
@Data
@ApiModel("CMS类目")
public class CmsItemsDTO {

    @ApiModelProperty(value = "id", required = true)
    private String id;

    @ApiModelProperty(value = "name", required = true)
    private String name;

    @ApiModelProperty(value = "sort")
    private String sort;

    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime updateTime;
}
