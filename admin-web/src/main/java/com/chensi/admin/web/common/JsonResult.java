package com.chensi.admin.web.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 响应返回消息体
 *
 * @author si.chen
 */
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonResult<T> {

    public static final String CODE_SUCCESS = "0";
    public static final String CODE_FAILED = "-1";
    public static final String CODE_NOT_LOGIN = "-2";
    private String code;
    private String message;
    private String description;
    private T data;

    private JsonResult(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private JsonResult(String code, String message, String description, T data) {
        this.code = code;
        this.message = message;
        this.description = description;
        this.data = data;
    }

    public static <T> JsonResult<T> ok(T data) {
        return new JsonResult(CODE_SUCCESS, "success", null, data);
    }

    public static JsonResult ok() {
        return JsonResult.ok(null);
    }

    public static JsonResult failed(String message) {
        return failed("", message);
    }

    public static JsonResult failed(BaseErrorCode baseErrorCode) {
        return new JsonResult(baseErrorCode.getCode(), baseErrorCode.getMessage());
    }

    public static JsonResult failed(BaseErrorCode baseErrorCode, String description) {
        return new JsonResult(baseErrorCode.getCode(), baseErrorCode.getMessage(), description, null);
    }

    public static JsonResult failed(String errorCode, String message) {
        return new JsonResult(errorCode, message, null, null);
    }

    public static JsonResult failed(String errorCode, String message, String description) {
        return new JsonResult(errorCode, message, description, null);
    }
}
