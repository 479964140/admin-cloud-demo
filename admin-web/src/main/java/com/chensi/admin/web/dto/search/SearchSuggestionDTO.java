package com.chensi.admin.web.dto.search;

import lombok.ToString;

import java.io.Serializable;

/**
 * @date 2019/6/179:46
 */
@ToString
public class SearchSuggestionDTO extends SearchPageDTO implements Serializable {

    private static final long serialVersionUID = 1916756417597073870L;
}
