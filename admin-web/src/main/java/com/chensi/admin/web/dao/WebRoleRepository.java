package com.chensi.admin.web.dao;

import com.chensi.admin.web.domain.WebRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @date 2019/6/20 14:09
 */

@Repository
public interface WebRoleRepository extends JpaRepository<WebRole, String>, JpaSpecificationExecutor<WebRole> {

    Optional<WebRole> findByNameAndDeleted(String name, Integer deleted);

    /**
     * 删除角色时删除中间表中role_id
     *
     * @param roleId 角色id
     */
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM web_role_menu WHERE role_id = ?1 ", nativeQuery = true)
    void delRoleId(String roleId);

    /**
     * 查询角色在中间表中是否被菜单对应
     *
     * @param roleId 角色id
     * @return
     */
    @Query(value = "SELECT DISTINCT role_id FROM web_role_menu WHERE role_id = ?1 ", nativeQuery = true)
    String queryRoleId(String roleId);

}
