package com.chensi.admin.web.feign.factory;

import com.chensi.admin.web.feign.CmsService;
import com.chensi.admin.web.feign.fallback.CmsFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author si.chen
 * @date 2019/7/29 10:35
 */
@Component
public class CmsFallbackFactory implements FallbackFactory<CmsService> {
    @Override
    public CmsService create(Throwable cause) {
        CmsFallbackImpl cmsFallback = new CmsFallbackImpl();
        cmsFallback.setCause(cause);
        return cmsFallback;
    }
}
