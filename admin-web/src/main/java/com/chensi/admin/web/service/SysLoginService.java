package com.chensi.admin.web.service;

import com.chensi.admin.web.domain.SysUserToken;
import com.chensi.admin.web.exception.BaseException;
import com.chensi.admin.web.exception.TokenException;
import com.chensi.admin.web.dto.SysLoginDTO;

import java.util.Set;

/**
 * @Author: si.chen
 * @Date: 2019/6/20 9:33
 */
public interface SysLoginService {

    /**
     * 登录
     *
     * @return token
     */
    SysLoginDTO login(SysLoginDTO sysLoginDTO) throws BaseException;

    /**
     * 根据token获取登录对象
     */
    SysUserToken getUserToken(String token) throws TokenException;

    /**
     * 查询
     */
    SysUserToken find(SysUserToken sysUserToken);

    /**
     * 用户退出
     */
    void logout(String userId) throws BaseException;

    /**
     * 修改密码
     */
    void updatePassword(SysLoginDTO sysLoginDTO) throws BaseException;

    /**
     * 获取用户权限集合
     */
    Set<String> getUserPermissions(String userId) throws BaseException;
}

