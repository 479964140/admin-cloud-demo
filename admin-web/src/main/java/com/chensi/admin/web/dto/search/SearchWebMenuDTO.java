package com.chensi.admin.web.dto.search;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @date 2019/6/20 14:03
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SearchWebMenuDTO extends SearchPageDTO {

    private String name;
}
