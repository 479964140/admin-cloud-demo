package com.chensi.admin.web.service;

import com.chensi.admin.web.domain.WebUser;
import com.chensi.admin.web.exception.BaseException;
import com.chensi.admin.web.dto.WebUserDTO;
import org.springframework.data.domain.Page;

/**
 * @title: WebUserService
 * @date 2019/6/1916:23
 */
public interface WebUserService {

    WebUser get(String id)throws BaseException;

    void delete(String id)throws BaseException;

    Page<WebUserDTO> page(String username,Integer pageNum, Integer pageSize)throws BaseException;

    WebUser create(WebUser webUser) throws BaseException;

    WebUser update(WebUser webUser) throws BaseException;

    WebUserDTO findById(String id)throws BaseException;

    void checkUser(String username) throws BaseException;

    WebUser findByUsername(String username);
}
