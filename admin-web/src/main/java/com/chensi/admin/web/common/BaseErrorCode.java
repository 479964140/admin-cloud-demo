package com.chensi.admin.web.common;

import lombok.Getter;

/**
 * @author si.chen
 */

@Getter
public enum BaseErrorCode {
    /**
     * Base Error Code
     */
    INVALID_PARAMETERS("00001", "Invalid Parameters"),
    ITEM_NOT_FOUND("00002", "Item Not Found"),
    ITEM_REPEAT("00003", "Item Repeat"),
    STATUS_NOT_MATCH("00004", "status not match"),
    ITEM_IS_USED("00005", "Item is used"),
    ITEM_NOT_MATCH("00006", "Item not matched"),
    /**
     * Web
     */
    UNKNOWN_ERROR("30001", "服务器忙，请联系管理员"),
    NOT_EXIST_USERNAME("30002", "不存在的用户"),
    ERROR_PASSWORD("30003", "密码错误"),
    LOGIN_FAIL_TOO_MUCH("30004", "密码错误次数超过上限"),
    LOCKED_USER("30005", "该用户被锁定，禁止登录"),
    AUTH_ERROR("30006", "认证失败"),
    TOKEN_ERROR("30007", "token失效，请重新登录"),
    UNAUTHORIZED("30008", "权限不足"),
    HYSTRIX_ERROR("30009", "远程服务不可用"),
    ;
    private String code;
    private String message;

    BaseErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
