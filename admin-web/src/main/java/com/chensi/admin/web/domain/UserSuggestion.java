package com.chensi.admin.web.domain;

import com.chensi.admin.web.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @date 2019/6/1417:33
 */

@Getter
@Setter
@Entity(name = "user_suggestion")
@Where(clause = "deleted = 0")
public class UserSuggestion implements Serializable {

    private static final long serialVersionUID = -6558320402449549398L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空")
    private String id;

    @NotBlank(message = "反馈内容不能为空")
    @Column(name = "content")
    private String content;

    @Column(name = "state")
    private Byte state;

    @Column(name = "operate_by")
    private String operateBy;

    @Column(name = "create_by")
    private String createBy;

    @LastModifiedDate
    @Column(name = "operate_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime operateTime;

    @CreatedDate
    @Column(name = "create_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @Column(name = "deleted")
    private Integer deleted = 0;

}
