package com.chensi.admin.web.controller;

import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.domain.CmsActivity;
import com.chensi.admin.web.domain.CmsContent;
import com.chensi.admin.web.domain.CmsItems;
import com.chensi.admin.web.domain.CmsTag;
import com.chensi.admin.web.dto.CmsItemsDTO;
import com.chensi.admin.web.dto.CmsTagDTO;
import com.chensi.admin.web.dto.search.SearchCmsActivityPageDTO;
import com.chensi.admin.web.dto.search.SearchCmsContentPageDTO;
import com.chensi.admin.web.dto.search.SearchCmsItemsPageDTO;
import com.chensi.admin.web.dto.search.SearchCmsTagPageDTO;
import com.chensi.admin.web.exception.BaseException;
import com.chensi.admin.web.feign.CmsService;
import com.chensi.admin.web.util.JsonUtil;
import com.chensi.admin.web.common.JsonResult;
import com.chensi.admin.web.dto.CmsActivityDTO;
import com.chensi.admin.web.dto.CmsContentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * @Date: 2019/6/24 9:51
 */

@RestController
@RequestMapping("/web/cms")
public class WebCmsController {

    private final CmsService cmsService;

    @Autowired
    @SuppressWarnings("all")
    public WebCmsController(CmsService cmsService) {
        this.cmsService = cmsService;
    }

    /***----------------------------ξ类目接口ξ--------------------------*/
    @GetMapping("/items")
    public ResponseEntity<JsonResult> list() throws BaseException {
        ResponseEntity<JsonResult<List<CmsItemsDTO>>> jsonResult = cmsService.listItems();
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @GetMapping("/items/{id}")
    public ResponseEntity<JsonResult> getItems(@PathVariable("id") String id) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.getItems(id);
        return JsonUtil.isRightOrNot(jsonResult);

    }

    @PostMapping("/items")
    ResponseEntity<JsonResult> saveItems(@Valid @RequestBody CmsItems cmsItems) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.saveItems(cmsItems);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @DeleteMapping("/items/{id}")
    ResponseEntity<JsonResult> deleteItems(@PathVariable("id") String id) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.deleteItems(id);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @PutMapping("/items/{id}")
    ResponseEntity<JsonResult> updateItems(
            @PathVariable("id") String id,
            @Validated(Constants.Update.class) @RequestBody CmsItems cmsItems)
            throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.updateItems(id, cmsItems);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @GetMapping("/items/page")
    ResponseEntity<JsonResult> pageItems(SearchCmsItemsPageDTO searchCmsItemsPageDTO) throws BaseException {
        return JsonUtil.isRightOrNot(cmsService.pageItems(
                searchCmsItemsPageDTO.getPageNum(),
                searchCmsItemsPageDTO.getPageSize(),
                searchCmsItemsPageDTO.getName()));
    }

    /*** ------------------------------ξ内容接口ξ-----------------------------*/

    @DeleteMapping("/content/{id}")
    ResponseEntity<JsonResult> deleteContent(@PathVariable("id") String id)
            throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.deleteContent(id);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @PutMapping("/content/{id}")
    ResponseEntity<JsonResult> updateContent(
            @PathVariable("id") String id,
            @Validated(Constants.Update.class) @RequestBody CmsContent cmsContent)
            throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.updateContent(id, cmsContent);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @GetMapping("/content/{id}")
    ResponseEntity<JsonResult> getContent(@PathVariable("id") String id) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.getContent(id);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @GetMapping("/content")
    ResponseEntity<JsonResult> listContent() throws BaseException {
        ResponseEntity<JsonResult<List<CmsContentDTO>>> jsonResult = cmsService.listContent();
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @PostMapping("/content")
    ResponseEntity<JsonResult> saveContent(
            @Valid @RequestBody CmsContent cmsContent) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.saveContent(cmsContent);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @GetMapping("/content/page")
    ResponseEntity<JsonResult> pageContent(SearchCmsContentPageDTO searchCmsContentPageDTO) throws BaseException {
        return JsonUtil.isRightOrNot(cmsService.pageContent(searchCmsContentPageDTO.getCmsItemsId(),
                searchCmsContentPageDTO.getPageNum(),
                searchCmsContentPageDTO.getPageSize(),
                searchCmsContentPageDTO.getTitle()));
    }


    /*** -----------------------------ξ标签接口ξ-------------------------------*/
    @DeleteMapping("/tag/{id}")
    ResponseEntity<JsonResult> deleteTag(@PathVariable("id") @NotNull String id)
            throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.deleteTag(id);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @PutMapping("/tag/{id}")
    ResponseEntity<JsonResult> updateTag(
            @PathVariable("id") String id,
            @Validated(Constants.Update.class) @RequestBody CmsTag cmsTag)
            throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.updateTag(id, cmsTag);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @GetMapping("/tag/{id}")
    ResponseEntity<JsonResult> getTag(@PathVariable("id") String id) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.getTag(id);
        return JsonUtil.isRightOrNot(jsonResult);
    }


    @GetMapping("/tag")
    ResponseEntity<JsonResult> listTag() throws BaseException {
        ResponseEntity<JsonResult<List<CmsTagDTO>>> jsonResult = cmsService.listTag();
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @PostMapping("/tag")
    ResponseEntity<JsonResult> saveTag(
            @Valid @RequestBody CmsTag cmsTag) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.saveTag(cmsTag);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @GetMapping("/tag/page")
    ResponseEntity<JsonResult> pageTag(SearchCmsTagPageDTO searchCmsTagPageDTO) throws BaseException {
        return JsonUtil.isRightOrNot(cmsService.pageTag(
                searchCmsTagPageDTO.getPageNum(),
                searchCmsTagPageDTO.getPageSize(),
                searchCmsTagPageDTO.getName()));
    }


    /*** -----------------------------ξ活动接口ξ-------------------------------*/

    @GetMapping("/activity")
    public ResponseEntity<JsonResult> listActivity() throws BaseException {
        ResponseEntity<JsonResult<List<CmsActivityDTO>>> jsonResult = cmsService.listActivity();
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @GetMapping("/activity/{id}")
    public ResponseEntity<JsonResult> getActivity(@PathVariable("id") String id) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.getActivity(id);
        return JsonUtil.isRightOrNot(jsonResult);

    }

    @PostMapping("/activity")
    ResponseEntity<JsonResult> saveActivity(@Valid @RequestBody CmsActivity cmsActivity) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.saveActivity(cmsActivity);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @DeleteMapping("/activity/{id}")
    ResponseEntity<JsonResult> deleteActivity(@PathVariable("id") String id) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.deleteActivity(id);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @PutMapping("/activity/{id}")
    ResponseEntity<JsonResult> updateActivity(
            @PathVariable("id") String id,
            @Validated(Constants.Update.class) @RequestBody CmsActivity cmsActivity)
            throws BaseException {
        ResponseEntity<JsonResult> jsonResult = cmsService.updateActivity(id, cmsActivity);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @GetMapping("/activity/page")
    ResponseEntity<JsonResult> pageActivity(SearchCmsActivityPageDTO searchCmsActivityPageDTO) throws BaseException {
        return JsonUtil.isRightOrNot(cmsService.pageActivity(
                searchCmsActivityPageDTO.getPageNum(),
                searchCmsActivityPageDTO.getPageSize(),
                searchCmsActivityPageDTO.getTitle()));
    }
}
