package com.chensi.admin.web.service;

import com.chensi.admin.web.domain.WebRole;
import com.chensi.admin.web.exception.BaseException;
import com.chensi.admin.web.dto.WebRoleDTO;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @date 2019/6/20 14:04
 */
public interface WebRoleService {

    List<WebRoleDTO> list();

    WebRoleDTO get(String id) throws BaseException;

    Page<WebRoleDTO> page(String name, Integer pageNum, Integer pageSize);

    WebRoleDTO create(WebRole webRole) throws BaseException;

    void update(WebRole webRole) throws BaseException;

    void delete(String id) throws BaseException;

}
