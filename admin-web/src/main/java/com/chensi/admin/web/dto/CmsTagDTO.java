package com.chensi.admin.web.dto;

import com.chensi.admin.web.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @date 2019/6/12 9:02
 */

@Data
@ApiModel("CMS标签")
public class CmsTagDTO {

    @ApiModelProperty(value = "主键", required = true)
    private String id;

    @ApiModelProperty(value = "标签名称", required = true)
    private String name;

    @ApiModelProperty(value = "排序顺序")
    private String sort;

    @ApiModelProperty(value = "展示状态")
    private Integer state;

    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime updateTime;

}
