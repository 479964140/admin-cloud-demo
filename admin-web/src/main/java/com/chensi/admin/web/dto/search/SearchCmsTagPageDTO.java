package com.chensi.admin.web.dto.search;

import lombok.Data;

/**
 * @date 2019/6/12 9:06
 */

@Data
public class SearchCmsTagPageDTO extends SearchPageDTO {

    private String name;
}
