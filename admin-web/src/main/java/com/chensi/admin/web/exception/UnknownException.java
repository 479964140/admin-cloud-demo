package com.chensi.admin.web.exception;

import com.chensi.admin.web.common.BaseErrorCode;

/**
 * @Author: si.chen
 * @Date: 2019/6/20 13:26
 */
public class UnknownException extends BaseException {
    public UnknownException(Exception e) {
        super(BaseErrorCode.UNKNOWN_ERROR.getCode(), BaseErrorCode.UNKNOWN_ERROR.getMessage(), e.getMessage());
    }
}
