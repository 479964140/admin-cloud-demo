package com.chensi.admin.web.exception;

public class NotLoginException extends BaseException {
    public NotLoginException(String description) {
        super("-1", "Authenticate failure", description);
    }
}
