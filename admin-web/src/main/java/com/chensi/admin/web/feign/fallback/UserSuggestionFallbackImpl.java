package com.chensi.admin.web.feign.fallback;

import com.chensi.admin.web.feign.UserSuggestionService;
import com.chensi.admin.web.common.BaseErrorCode;
import com.chensi.admin.web.common.JsonResult;
import com.chensi.admin.web.domain.UserSuggestion;
import com.chensi.admin.web.exception.BaseException;
import lombok.Setter;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

/**
 * @author si.chen
 * @date 2019/7/29 10:59
 */
@Component
public class UserSuggestionFallbackImpl implements UserSuggestionService {

    @Setter
    private Throwable cause;

    @Override
    public ResponseEntity<JsonResult> save(@Valid UserSuggestion userSuggestion) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> delete(String id) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> update(String id, UserSuggestion userSuggestion) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }

    @Override
    public ResponseEntity<JsonResult> page(Integer pageNum, Integer pageSize) throws BaseException {
        throw new BaseException(BaseErrorCode.HYSTRIX_ERROR, cause.getMessage());
    }
}
