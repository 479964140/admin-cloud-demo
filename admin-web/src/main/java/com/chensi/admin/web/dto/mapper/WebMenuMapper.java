package com.chensi.admin.web.dto.mapper;

import com.chensi.admin.web.domain.WebMenu;
import com.chensi.admin.web.dto.WebMenuDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;

/**
 * @date 2019/6/20 14:02
 */

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface WebMenuMapper extends BaseMapper<WebMenu, WebMenuDTO> {

    void update(@MappingTarget WebMenu old, WebMenu nr);
}
