package com.chensi.admin.web.controller.api;

import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.common.JsonResult;
import com.chensi.admin.web.dto.SysLoginDTO;
import com.chensi.admin.web.exception.BaseException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: si.chen
 * @Date: 2019/6/19 14:42
 */
@Api(tags = "Web登录接口文档")
@RequestMapping("user")
public interface SysLoginApi {

    @ApiOperation(value = "登录", notes = "入参以及验证规则：账号、密码非空", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("login")
    ResponseEntity<JsonResult> login(@Validated({Constants.Login.class})
                                     @RequestBody SysLoginDTO sysLoginDTO) throws BaseException;

    @ApiOperation(value = "退出", notes = "用户退出", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("logout")
    ResponseEntity<JsonResult> logout() throws BaseException;

    @ApiOperation(value = "修改密码", notes = "入参以及验证规则：新密码、旧密码非空", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("updatePassword")
    ResponseEntity<JsonResult> updatePassword(@Validated({Constants.Password.class})
                                              @RequestBody SysLoginDTO sysLoginDTO) throws BaseException;
}

