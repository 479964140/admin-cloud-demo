package com.chensi.admin.web.controller.api;

import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.common.JsonResult;
import com.chensi.admin.web.domain.WebRole;
import com.chensi.admin.web.dto.WebMenuDTO;
import com.chensi.admin.web.dto.WebRoleDTO;
import com.chensi.admin.web.dto.search.SearchWebRoleDTO;
import com.chensi.admin.web.exception.BaseException;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author si.chen
 * @date 2019/6/20 16:51
 */

@Api(tags = "WEB Role API")
@RequestMapping("/web/role")
public interface WebRoleApi {

    /**
     * 根据主键id删除角色
     *
     * @param id 主键id
     * @return json
     * @throws BaseException 异常
     */
    @ApiOperation(value = "删除角色", notes = "删除角色。验证规则：id不可为空", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色Id", required = true, paramType = "path", dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "成功返回", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "WebRole: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @DeleteMapping("/{id}")
    ResponseEntity<JsonResult> delete(@PathVariable("id") String id)
            throws BaseException;

    /**
     * 根据Id更新角色
     *
     * @param id      主键id
     * @param webRole 角色信息数据
     * @return 更新状态
     * @throws BaseException 更新异常信息
     */
    @ApiOperation(value = "更新角色", notes = "根据id更新角色。验证规则：id不能为空", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色Id", required = true, paramType = "path", dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "成功返回", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "WebRole: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PutMapping("/{id}")
    ResponseEntity<JsonResult> update(@PathVariable("id") String id,
                                      @Validated(Constants.Update.class) @RequestBody WebRole webRole)
            throws BaseException;

    /**
     * 获取角色详情
     *
     * @param id 主键id
     * @return 角色详细信息
     * @throws BaseException 查询异常信息
     */
    @ApiOperation(value = "获取角色详情", notes = "根据id获取角色信息。验证规则：id不能为空", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "角色ID", required = true, paramType = "path", dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "成功返回", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "WebRole: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @GetMapping("/{id}")
    ResponseEntity<JsonResult> get(@PathVariable("id") String id) throws BaseException;

    /**
     * 查询所有菜单
     *
     * @return 菜单集合
     */
    @GetMapping("")
    @ApiOperation(value = "获取所有角色", notes = "查询所有角色", response = WebRoleDTO.class)
    ResponseEntity<JsonResult<List<WebRoleDTO>>> list();

    /**
     * 分页查询，模糊查询
     *
     * @param searchWebRoleDTO 分页参数
     * @return 菜单集合
     */
    @GetMapping("/page")
    @ApiOperation(value = "获取菜单分页列表", notes = "根据名称模库查询", response = WebMenuDTO.class)
    ResponseEntity<JsonResult<Page<WebRoleDTO>>> page(SearchWebRoleDTO searchWebRoleDTO);

    /**
     * 创建角色
     *
     * @param webRole 角色信息
     * @return 返回创建成功的角色信息
     * @throws BaseException 创建异常
     */
    @ApiOperation(value = "创建角色", notes = "创建角色", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "成功返回", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "WebRole: name is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("")
    ResponseEntity<JsonResult> save(
            @Valid @RequestBody WebRole webRole) throws BaseException;
}
