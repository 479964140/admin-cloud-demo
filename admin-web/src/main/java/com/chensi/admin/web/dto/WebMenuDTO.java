package com.chensi.admin.web.dto;

import com.chensi.admin.web.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @date 2019/6/20 13:49
 */

@Data
@ApiModel("菜单")
public class WebMenuDTO {

    @ApiModelProperty(value = "主键", required = true)
    private String id;

    @ApiModelProperty(value = "父级菜单", required = true)
    private String parentId;

    @ApiModelProperty(value = "菜单名称", required = true)
    private String name;

    @ApiModelProperty(value = "url")
    private String url;

    @ApiModelProperty(value = "权限", required = true)
    private String perms;

    @ApiModelProperty(value = "权限类型", required = true)
    private Integer type;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "排序")
    private String sort;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime updateTime;
}
