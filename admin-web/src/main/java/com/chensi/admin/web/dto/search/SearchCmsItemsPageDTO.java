package com.chensi.admin.web.dto.search;

import lombok.Data;

/**
 * @Date: 2019/6/10 17:40
 */
@Data
public class SearchCmsItemsPageDTO extends SearchPageDTO {

    private String name;

}
