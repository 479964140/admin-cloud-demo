package com.chensi.admin.web.dto;

import com.chensi.admin.web.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @date 2019/7/2310:59
 */
@Data
@ApiModel("学校信息")
public class EduSchoolDTO implements Serializable {

    private static final long serialVersionUID = -2109985310918151864L;

    @ApiModelProperty(value = "id", required = true)
    private String id;

    @ApiModelProperty(value = "注册编码", required = true)
    private String code;

    @ApiModelProperty(value = "名称", required = true)
    private String name;

    @ApiModelProperty(value = "简介", required = true)
    private String introduction;

    @ApiModelProperty(value = "学校性质", required = true)
    private String nature;

    @ApiModelProperty(value = "状态", required = true)
    private String state;

    @ApiModelProperty(value = "联系人", required = true)
    private String contact;

    @ApiModelProperty(value = "法律人", required = true)
    private String leader;

    @ApiModelProperty(value = "联系电话", required = true)
    private String phone;

    @ApiModelProperty(value = "地址", required = true)
    private String address;

    @ApiModelProperty(value = "成立时间", required = true)
    private String establish;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "创建人", required = true)
    private String createBy;

    @ApiModelProperty(value = "处理人", required = true)
    private String operateBy;

    @ApiModelProperty(value = "是否删除状态")
    private Integer deleted;

    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime updateTime;
}
