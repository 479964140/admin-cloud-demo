package com.chensi.admin.web.domain;

import com.chensi.admin.web.common.Constants;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @date 2019/6/1914:52
 */

@Getter
@Setter
@Entity(name = "web_user")
@Where(clause = "deleted = 0")
public class WebUser extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 6881791624947003778L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "id required", groups = Constants.Update.class)
    private String id;

    @NotBlank(message = "用户名不能为空",groups = {Constants.Update.class})
    @Column(name = "username")
    private String username;

    @NotBlank(message = "密码不能为空")
    @Column(name = "password")
    private String password;

    @Column(name = "salt")
    private String salt;

    @NotBlank(message = "邮件不能为空" ,groups = {Constants.Update.class})
    @Column(name = "email")
    private String email;

    @NotBlank(message = "电话不能为空",groups = {Constants.Update.class})
    @Column(name = "phone")
    private String phone;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private WebRole webRole;

    /**
     * 正常：0 锁定：1
     */
    @Column(name = "state")
    private Integer state = Constants.USER_STATE_NORMAL;


}
