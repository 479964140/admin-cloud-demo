package com.chensi.admin.web.feign.factory;

import com.chensi.admin.web.feign.UserSuggestionService;
import com.chensi.admin.web.feign.fallback.UserSuggestionFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author si.chen
 * @date 2019/7/29 10:35
 */
@Component
public class UserSuggestionFallbackFactory implements FallbackFactory<UserSuggestionService> {
    @Override
    public UserSuggestionService create(Throwable cause) {
        UserSuggestionFallbackImpl userSuggestionFallback = new UserSuggestionFallbackImpl();
        userSuggestionFallback.setCause(cause);
        return userSuggestionFallback;
    }
}
