package com.chensi.admin.web.exception;

import com.chensi.admin.web.common.BaseErrorCode;

/**
 * @Author: si.chen
 * @Date: 2019/6/24 9:50
 */
public class TokenException extends BaseException {
    public TokenException() {
        super(BaseErrorCode.TOKEN_ERROR.getCode(), BaseErrorCode.TOKEN_ERROR.getMessage(),
                BaseErrorCode.TOKEN_ERROR.getMessage());
    }
}
