package com.chensi.admin.web.dto.mapper;

import com.chensi.admin.web.domain.WebRole;
import com.chensi.admin.web.dto.WebRoleDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;

/**
 * @date 2019/6/20 14:00
 */

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface WebRoleMapper extends BaseMapper<WebRole, WebRoleDTO> {

    void update(@MappingTarget WebRole old, WebRole nr);

}
