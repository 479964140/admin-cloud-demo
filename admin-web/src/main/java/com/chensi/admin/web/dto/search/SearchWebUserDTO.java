package com.chensi.admin.web.dto.search;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @date 2019/6/1915:50
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SearchWebUserDTO extends SearchPageDTO {

    private String username;
}
