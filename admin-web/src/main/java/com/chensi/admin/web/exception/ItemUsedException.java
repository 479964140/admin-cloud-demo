package com.chensi.admin.web.exception;


import com.chensi.admin.web.common.BaseErrorCode;

public class ItemUsedException extends BaseException {
    public ItemUsedException(String description) {
        super(BaseErrorCode.ITEM_IS_USED.getCode(), BaseErrorCode.ITEM_IS_USED.getMessage(), description);
    }
}
