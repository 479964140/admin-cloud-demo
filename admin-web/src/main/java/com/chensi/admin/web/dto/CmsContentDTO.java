package com.chensi.admin.web.dto;

import com.chensi.admin.web.domain.CmsItems;
import com.chensi.admin.web.domain.CmsTag;
import com.chensi.admin.web.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * @date 2019/6/11 17:24
 */

@Data
@ApiModel("CMS内容")
public class CmsContentDTO {

    @ApiModelProperty(value = "id", required = true)
    private String id;

    @ApiModelProperty(value = "外键-cmsItemsId", required = true)
    private CmsItems cmsItems;

    @ApiModelProperty(value = "标签信息-cmsTag", required = true)
    private Set<CmsTag> cmsTags;

    @ApiModelProperty(value = "内容标题-title", required = true)
    private String title;

    @ApiModelProperty(value = "缩略图-thumbnail", required = true)
    private String thumbnail;

    @ApiModelProperty(value = "内容-content", required = true)
    private String content;

    @ApiModelProperty(value = "发布状态-state")
    private Integer state;

    @ApiModelProperty(value = "阅读量")
    private Integer readCount;

    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime updateTime;


}
