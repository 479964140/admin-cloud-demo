package com.chensi.admin.web.dto.search;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @date 2019/6/11 17:37
 */

@Data
public class SearchCmsContentPageDTO extends SearchPageDTO {

    private String title;

    @NotBlank(message = "条目id不能为空")
    private String cmsItemsId;
}
