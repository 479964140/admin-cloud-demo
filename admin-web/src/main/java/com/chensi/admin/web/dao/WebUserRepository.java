package com.chensi.admin.web.dao;

import com.chensi.admin.web.domain.WebUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @date 2019/6/1916:32
 */
@Repository
public interface WebUserRepository extends JpaRepository<WebUser, String>, JpaSpecificationExecutor<WebUser> {

    WebUser findByUsername(String username);

    WebUser findByUsernameAndIdNot(String username, String id);

    @Modifying
    @Query(value = "update web_user set role_id = null where role_id = ?1 ", nativeQuery = true)
    void updRoleIdToNull(String roleId);
}
