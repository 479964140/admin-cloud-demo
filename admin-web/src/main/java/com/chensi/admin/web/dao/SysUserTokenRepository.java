package com.chensi.admin.web.dao;

import com.chensi.admin.web.domain.SysUserToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: si.chen
 * @Date: 2019/6/20 13:43
 */
@Transactional(rollbackFor = Exception.class)
@Repository
public interface SysUserTokenRepository extends JpaRepository<SysUserToken, String>, JpaSpecificationExecutor<SysUserToken> {

    /**
     * 删除
     *
     * @param userId 用户id
     */
    void deleteByUserId(String userId);

}
