package com.chensi.admin.web.dto.mapper;

import com.chensi.admin.web.domain.WebUser;
import com.chensi.admin.web.dto.WebUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;

/**
 * @date 2019/6/1714:28
 */

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface WebUserMapper extends BaseMapper<WebUser, WebUserDTO> {

    void update(@MappingTarget WebUser old, WebUser nr);
}