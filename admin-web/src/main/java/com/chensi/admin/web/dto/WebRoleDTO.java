package com.chensi.admin.web.dto;

import com.chensi.admin.web.domain.WebMenu;
import com.chensi.admin.web.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * @date 2019/6/20 13:31
 */

@Data
@ApiModel("角色")
public class WebRoleDTO {

    @ApiModelProperty(value = "主键", required = true)
    private String id;

    @ApiModelProperty(value = "角色", required = true)
    private String name;

    @ApiModelProperty(value = "菜单", required = true)
    private Set<WebMenu> webMenus;

    @ApiModelProperty(value = "创建人", required = true)
    private String createBy;

    @ApiModelProperty(value = "修改人", required = true)
    private String operateBy;

    @ApiModelProperty(value = "是否删除", required = true)
    private Integer deleted;

    @ApiModelProperty(value = "创建时间", required = true)
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", required = true)
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime updateTime;
}
