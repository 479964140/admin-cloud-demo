package com.chensi.admin.web.exception;

import com.chensi.admin.web.common.BaseErrorCode;

/**
 * @Author: si.chen
 * @Date: 2019/6/20 10:53
 */
public class UserLockedException extends BaseException {
    public UserLockedException() {
        super(BaseErrorCode.LOCKED_USER.getCode(), BaseErrorCode.LOCKED_USER.getMessage(),
                BaseErrorCode.LOCKED_USER.getMessage());
    }

}
