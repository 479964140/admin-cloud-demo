package com.chensi.admin.web.oauth2;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @Author: si.chen
 * @Date: 2019/6/20 16:17
 */
public class OAuth2Token implements AuthenticationToken {

    private String token;

    public OAuth2Token(String token) {
        this.token = token;
    }

    @Override
    public String getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

}
