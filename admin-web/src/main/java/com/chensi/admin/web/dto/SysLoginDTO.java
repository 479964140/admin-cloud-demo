package com.chensi.admin.web.dto;

import com.chensi.admin.web.common.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Date: 2019/6/19 15:31
 */
@Data
public class SysLoginDTO implements Serializable {

    private static final long serialVersionUID = -5015225793240065050L;

    @NotBlank(message = "用户名不为空", groups = {Constants.Login.class})
    @ApiModelProperty(value = "用户名")
    private String username;

    @NotBlank(message = "密码不为空", groups = {Constants.Login.class, Constants.Password.class})
    @ApiModelProperty(value = "密码")
    private String password;

    @NotBlank(message = "旧密码不为空", groups = {Constants.Password.class})
    @ApiModelProperty(value = "旧密码")
    private String oldPassword;

    private String userId;

    private String token;
}
