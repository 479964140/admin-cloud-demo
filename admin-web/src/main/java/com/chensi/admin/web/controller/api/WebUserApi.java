package com.chensi.admin.web.controller.api;

import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.common.JsonResult;
import com.chensi.admin.web.domain.WebUser;
import com.chensi.admin.web.dto.WebUserDTO;
import com.chensi.admin.web.dto.search.SearchWebUserDTO;
import com.chensi.admin.web.exception.BaseException;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 用户信息接口
 *
 * @author si.chen
 */
@Api(tags = "User Info API")
@RequestMapping("/web/user")
public interface WebUserApi {

    @ApiOperation(value = "用户新增", notes = "用户新增，入参以及验证规则：手机号，密码非空等等", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("")
    ResponseEntity<JsonResult> save(@RequestBody @Valid WebUser entity) throws BaseException;

    @ApiOperation(value = "用户删除", notes = "用户删除。入参以及验证规则：id不可为空等等", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, paramType = "path",
                    dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "items: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @DeleteMapping("/{id}")
    ResponseEntity<JsonResult> delete(@PathVariable("id") String id)
            throws BaseException;

    @GetMapping("/page")
    @ApiOperation(value = "获取用户列表", notes = "根据用户反馈模库查询", response = WebUserDTO.class)
    ResponseEntity<JsonResult<Page<WebUserDTO>>> page(SearchWebUserDTO searchWebUserDTO) throws BaseException;

    @ApiOperation(value = "更新用户", notes = "根据id修改用户意见反馈。验证规则：id不能为空等等", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户意见反馈ID", required = true, paramType = "path", dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "成功返回", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "CmsContent: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PutMapping("/{id}")
    ResponseEntity<JsonResult> update(@PathVariable("id") String id, @Validated({Constants.Update.class}) @RequestBody WebUser webUser) throws BaseException;

    @ApiOperation(value = "通过id获取用户内容", notes = "通过id获取用户内容", response = WebUserDTO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, paramType = "path",
                    dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "items: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @GetMapping("/{id}")
    ResponseEntity<JsonResult<WebUserDTO>> webUser(@PathVariable("id") String id) throws BaseException;

}
