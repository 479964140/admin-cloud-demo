package com.chensi.admin.web.dto.search;

import lombok.Data;

/**
 * @date 2019/6/17 9:49
 */

@Data
public class SearchCmsActivityPageDTO extends SearchPageDTO {

    private String title;
}
