package com.chensi.admin.web.domain;

import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @date 2019/6/14 15:00
 */

@Getter
@Setter
@Entity(name = "cms_activity")
@Where(clause = "deleted = 0")
public class CmsActivity extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "id required", groups = Constants.Update.class)
    private String id;

    private String title;

    private String thumbnail;

    @Column(name = "start_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime startTime;

    @Column(name = "end_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime endTime;

    private String address;

    @Column(name = "activity_state")
    private Integer activityState;

    private String text;

    @Column(name = "approval_state")
    private Integer approvalState;

}
