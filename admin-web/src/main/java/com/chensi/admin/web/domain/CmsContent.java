package com.chensi.admin.web.domain;

import com.chensi.admin.web.common.Constants;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

/**
 * @date 2019/6/11 17:16
 */

@Getter
@Setter
@Entity(name = "cms_content")
@Where(clause = "deleted = 0")
public class CmsContent extends BaseEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "id required", groups = Constants.Update.class)
    private String id;

    @ManyToOne
    @JoinColumn(name = "cms_items_id")
    private CmsItems cmsItems;

    @ManyToMany
    @JoinTable(name = "cms_content_tag",
            joinColumns = @JoinColumn(name = "cms_content_id", referencedColumnName = "id")
            , inverseJoinColumns = @JoinColumn(name = "cms_tag_id", referencedColumnName = "id"))
    private Set<CmsTag> cmsTags = new HashSet<>();

    @NotBlank(message = "标题不能为空")
    @Length(max = 50, min = 5, message = "标题最长不能超过20个字，最短不少于10个字")
    private String title;

    @NotBlank(message = "缩略图不能为空")
    private String thumbnail;

    @NotBlank(message = "内容不能为空")
    private String content;

    private Integer state;


}
