package com.chensi.admin.web.dto.search;

import lombok.Data;

import java.io.Serializable;

/**
 * @date 2019/7/2311:29
 */
@Data
public class SearchEduSchoolDTO extends SearchPageDTO implements Serializable {

    private static final long serialVersionUID = -463130566155848143L;

    private String name;
}
