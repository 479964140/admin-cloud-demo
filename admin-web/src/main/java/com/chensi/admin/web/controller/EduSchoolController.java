package com.chensi.admin.web.controller;

import com.chensi.admin.web.common.Constants;
import com.chensi.admin.web.domain.EduSchool;
import com.chensi.admin.web.dto.search.SearchEduSchoolDTO;
import com.chensi.admin.web.exception.BaseException;
import com.chensi.admin.web.feign.EduSchoolService;
import com.chensi.admin.web.util.JsonUtil;
import com.chensi.admin.web.common.JsonResult;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author si.chen
 * @title: EduSchoolController
 * @date 2019/7/299:34
 */
@Controller
@RequestMapping("/edu/school")
public class EduSchoolController {

    private final EduSchoolService eduSchoolService;

    public EduSchoolController(EduSchoolService eduSchoolService) {
        this.eduSchoolService = eduSchoolService;
    }

    /**
     * 创建学校信息
     * @param eduSchool eduSchool
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 基本异常
     */
    @PostMapping("")
    ResponseEntity<JsonResult> create(@Valid @RequestBody EduSchool eduSchool) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = eduSchoolService.create(eduSchool);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    /**
     * 删除学校信息
     * @param id id
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 基本异常
     */
    @DeleteMapping("/{id}")
    ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException{
        ResponseEntity<JsonResult> jsonResult = eduSchoolService.delete(id);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    /**
     * 更新学校信息
     * @param id id
     * @param eduSchool eduSchool
     * @return ResponseEntity<JsonResult>
     * @throws BaseException 基本异常
     */
    @PutMapping("/{id}")
    ResponseEntity<JsonResult> update(@PathVariable("id") String id, @Validated({Constants.Update.class})
    @RequestBody EduSchool eduSchool) throws BaseException {
        ResponseEntity<JsonResult> jsonResult = eduSchoolService.update(id,eduSchool);
        return JsonUtil.isRightOrNot(jsonResult);
    }

    @GetMapping("/page")
    ResponseEntity<JsonResult> page(SearchEduSchoolDTO searchEduSchoolDTO) throws BaseException {
        return JsonUtil.isRightOrNot(eduSchoolService.page(
                searchEduSchoolDTO.getPageNum(),
                searchEduSchoolDTO.getPageSize(),
                searchEduSchoolDTO.getName()
             ));
    }

    /**
     * 获取学校信息
     * @return  ResponseEntity<JsonResult>
     * @throws BaseException 基本异常
     */
    @GetMapping("")
    ResponseEntity<JsonResult> list() throws BaseException{
        ResponseEntity<JsonResult> jsonResult = eduSchoolService.list();
        return JsonUtil.isRightOrNot(jsonResult);
    }
}
