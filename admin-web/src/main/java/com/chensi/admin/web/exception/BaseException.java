package com.chensi.admin.web.exception;

import com.chensi.admin.web.common.BaseErrorCode;
import lombok.Getter;

/**
 * @author si.chen
 */
@Getter
public class BaseException extends Exception {
    private String errorCode;
    private String errorMessage;

    public BaseException(String errorCode, String errorMessage, String description) {
        super(description);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public BaseException(BaseErrorCode baseErrorCode) {
        super(baseErrorCode.getMessage());
        this.errorCode = baseErrorCode.getCode();
        this.errorMessage = baseErrorCode.getMessage();
    }

    public BaseException(BaseErrorCode baseErrorCode, String description) {
        super(description);
        this.errorCode = baseErrorCode.getCode();
        this.errorMessage = baseErrorCode.getMessage();
    }
}
