package com.chensi.admin.web;

import com.chensi.admin.web.dao.WebUserRepository;
import com.chensi.admin.web.domain.WebUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyApplication {

    @Autowired
    private WebUserRepository webUserRepository;

    @Test
    public void contextLoads() {
    }

    @Test
    public void test() {
        WebUser webUser = (WebUser) SecurityUtils.getSubject().getPrincipal();
        System.out.println(webUser.getUsername());
    }

    @Test
    public void test2() {
        WebUser webUser = webUserRepository.findByUsername("bbb");
        System.out.println(webUser.getUsername());
    }

    @Test
    @RequiresPermissions(value = {""})
    public void test3() {
        System.out.println(0);
    }

}
