package com.chensi.admin.web;

import org.apache.shiro.crypto.hash.Sha256Hash;
import org.junit.Test;

/**
 * @Author: si.chen
 * @Date: 2019/6/21 10:30
 */
public class MyTest {

    @Test
    public void test() {
        String str = new Sha256Hash("123456", "rdoXZbFf0Ld2laozhoY1").toHex();
        System.out.println(str);
    }
}
