drop table if exists suggestion_picture;


/*==============================================================*/
/* Table: suggestion_picture                                    */
/*==============================================================*/
create table suggestion_picture (
   id                   VARCHAR(32)          not null,
   suggestion_id        VARCHAR(32)          null,
   picture_url          TEXT                 null,
   deleted              INT2                 null,
   constraint PK_SUGGESTION_PICTURE primary key (id)
);

comment on table suggestion_picture is
'图片保存';

comment on column suggestion_picture.id is
'主键';

comment on column suggestion_picture.suggestion_id is
'关联id';

comment on column suggestion_picture.picture_url is
'图片url';

comment on column suggestion_picture.deleted is
'是否删除';
