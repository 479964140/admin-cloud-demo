/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     2019/6/14 16:54:32                           */
/*==============================================================*/




drop table if exists user_suggestion;

/*==============================================================*/
/* Table: common_picture                                        */
/*==============================================================*/
CREATE TABLE "public"."user_suggestion" (
"id" varchar(32) COLLATE "default" NOT NULL,
"content" varchar(512) COLLATE "default",
"create_by" varchar(32) COLLATE "default",
"operate_by" varchar(32) COLLATE "default",
"operate_time" timestamp(6),
"state" int2,
"create_time" timestamp(6),
"deleted" int2,
CONSTRAINT "pk_user_suggestion" PRIMARY KEY ("id")
)
WITH (OIDS=FALSE)
;

ALTER TABLE "public"."user_suggestion" OWNER TO "postgres";

COMMENT ON TABLE "public"."user_suggestion" IS '意见反馈内容';

COMMENT ON COLUMN "public"."user_suggestion"."id" IS '主键';

COMMENT ON COLUMN "public"."user_suggestion"."content" IS '内容';

COMMENT ON COLUMN "public"."user_suggestion"."create_by" IS '创建人';

COMMENT ON COLUMN "public"."user_suggestion"."operate_by" IS '处理人';

COMMENT ON COLUMN "public"."user_suggestion"."operate_time" IS '处理时间';

COMMENT ON COLUMN "public"."user_suggestion"."state" IS '状态';

COMMENT ON COLUMN "public"."user_suggestion"."create_time" IS '创建时间';

COMMENT ON COLUMN "public"."user_suggestion"."deleted" IS '是否删除';