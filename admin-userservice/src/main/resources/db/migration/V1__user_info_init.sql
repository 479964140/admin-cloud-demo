drop table if exists user_info;

/*==============================================================*/
/* Table: user_info                                             */
/*==============================================================*/
create table user_info (
   id                   VARCHAR(32)          not null,
   real_name            VARCHAR(32)          null,
   sex                  INT2                 null,
   province             VARCHAR(32)          null,
   city                 VARCHAR(32)          null,
   phone                VARCHAR(32)          null,
   password             VARCHAR(32)          null,
   id_card_num          VARCHAR(32)          null,
   real_name_type       INT2                 null,
   real_name_status     INT2                 null,
   avatar               VARCHAR(256)         null,
   create_time          TIMESTAMP            null,
   update_time          TIMESTAMP            null,
   deleted              BOOL                 null,
   constraint PK_USER_INFO primary key (id)
);

comment on table user_info is
'用户信息';

comment on column user_info.id is
'UUID';

comment on column user_info.real_name is
'真实姓名';

comment on column user_info.sex is
'1:男 2:女';

comment on column user_info.phone is
'手机号';

comment on column user_info.password is
'用户密码';

comment on column user_info.id_card_num is
'证件号码';

comment on column user_info.real_name_type is
'1：支付宝实名 2：聚合三网实名 3：身份证实名';

comment on column user_info.real_name_status is
'0：未认证  1：已认证';

comment on column user_info.avatar is
'文件地址';

comment on column user_info.create_time is
'创建时间';

comment on column user_info.update_time is
'修改时间';

comment on column user_info.deleted is
'0：否  1：是';
