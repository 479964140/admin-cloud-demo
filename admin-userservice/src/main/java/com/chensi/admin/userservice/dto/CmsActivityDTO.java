package com.chensi.admin.userservice.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.chensi.admin.userservice.utils.DateUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author si.chen
 * @date 2019/6/14 16:27
 */

@Data
@ApiModel("CMS活动")
public class CmsActivityDTO implements Serializable {

    private static final long serialVersionUID = 1714530515729096270L;

    @ApiModelProperty(value = "id", required = true)
    private String id;

    @ApiModelProperty(value = "title", required = true)
    private String title;

    @ApiModelProperty(value = "thumbnail", required = true)
    private String thumbnail;

    @ApiModelProperty(value = "startTime", required = true)
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime startTime;

    @ApiModelProperty(value = "endTime", required = true)
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime endTime;

    @ApiModelProperty(value = "address", required = true)
    private String address;

    @ApiModelProperty(value = "activityState", required = true)
    private Integer activityState;

    @ApiModelProperty(value = "text", required = true)
    private String text;

    @ApiModelProperty(value = "approvalState", required = true)
    private Integer approvalState;
}
