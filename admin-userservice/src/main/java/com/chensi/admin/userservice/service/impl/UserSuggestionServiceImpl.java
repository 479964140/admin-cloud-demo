package com.chensi.admin.userservice.service.impl;

import com.chensi.admin.userservice.dao.SuggestionPictureRepository;
import com.chensi.admin.userservice.dao.UserSuggestionRepository;
import com.chensi.admin.userservice.dto.UserSuggestionDTO;
import com.chensi.admin.userservice.dto.mapper.UserSuggestionMapper;
import com.chensi.admin.userservice.service.SuggestionPictureService;
import com.chensi.admin.userservice.service.UserSuggestionService;
import com.google.common.collect.Lists;
import com.chensi.admin.userservice.common.Constants;
import com.chensi.admin.userservice.domain.SuggestionPicture;
import com.chensi.admin.userservice.domain.UserSuggestion;
import com.chensi.admin.userservice.exception.BaseException;
import com.chensi.admin.userservice.utils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author si.chen
 * @date 2019/6/1713:10
 */
@Service
public class UserSuggestionServiceImpl implements UserSuggestionService {

    private final UserSuggestionMapper userSuggestionMapper;

    private final UserSuggestionRepository userSuggestionRepository;

    private final SuggestionPictureRepository suggestionPictureRepository;

    private final SuggestionPictureService suggestionPictureService;

    private Integer zero = 0;

    private Integer positive = 1;

    @Autowired
    public UserSuggestionServiceImpl(UserSuggestionMapper userSuggestionMapper,
                                     UserSuggestionRepository userSuggestionRepository,
                                     SuggestionPictureRepository suggestionPictureRepository,
                                     SuggestionPictureService suggestionPictureService) {
        this.userSuggestionMapper = userSuggestionMapper;
        this.userSuggestionRepository = userSuggestionRepository;
        this.suggestionPictureRepository = suggestionPictureRepository;
        this.suggestionPictureService = suggestionPictureService;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(UserSuggestion userSuggestion) {
        UserSuggestion suggestion = userSuggestionRepository.save(userSuggestion);
        Set<SuggestionPicture> SuggestionPictures = userSuggestion.getSuggestionPictures();
        for(SuggestionPicture suggestionPicture : SuggestionPictures){
            suggestionPicture.setUserSuggestion(suggestion);
            suggestionPictureRepository.save(suggestionPicture);
         }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String id) throws BaseException {
        UserSuggestion entity = this.get(id);
        entity.setDeleted(Constants.DELETED_YES);
        userSuggestionRepository.save(entity);
        List<SuggestionPicture> suggestionPictures = suggestionPictureService.get(entity);
        for(SuggestionPicture suggestionPicture : suggestionPictures){
            suggestionPicture.setDeleted(Constants.DELETED_YES);
            suggestionPictureRepository.save(suggestionPicture);
        }
    }

    @Override
    public UserSuggestion get(String id) {
        return userSuggestionRepository.findById(id).orElse(null);
    }

    @Override
    public UserSuggestion update(UserSuggestion userSuggestion) {
        UserSuggestion entity = this.get(userSuggestion.getId());
        BeanUtils.copyNotNullProperties(userSuggestion, entity);
        return userSuggestionRepository.save(entity);
    }

    @Override
    public Page<UserSuggestionDTO> page(Integer pageNum, Integer pageSize) {
        pageNum = Optional.ofNullable(pageNum).filter(n -> n >= zero).orElse(Constants.PAGE_NUM);
        pageSize = Optional.ofNullable(pageSize).filter(n -> n > positive).orElse(Constants.PAGE_SIZE);
        return userSuggestionRepository.findAll(
                (root, query, cb) -> {
                    Predicate p1 = cb.equal(root.get("deleted"), Constants.DELETED_NO);
                    List<Predicate> predicates = Lists.newArrayList(p1);
                    query.where(predicates.toArray(new Predicate[0]));
                    return query.getRestriction();
                }, PageRequest.of(pageNum, pageSize))
                .map(userSuggestionMapper::toDTO);
    }

}
