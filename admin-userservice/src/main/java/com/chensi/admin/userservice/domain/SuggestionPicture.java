package com.chensi.admin.userservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.chensi.admin.userservice.common.Constants;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author si.chen
 * @date 2019/7/516:15
 */

@Getter
@Setter
@Entity(name = "suggestion_picture")
@Where(clause = "deleted = 0")
@EntityListeners(AuditingEntityListener.class)
public class SuggestionPicture implements Serializable {

    private static final long serialVersionUID = -9141451444824695053L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    private String id;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "suggestion_id")
    private UserSuggestion userSuggestion;

    @NotBlank(message = "图片地址")
    @Column(name = "picture_url")
    private String pictureUrl;

    @Column(name = "deleted")
    private Integer deleted = 0;
}
