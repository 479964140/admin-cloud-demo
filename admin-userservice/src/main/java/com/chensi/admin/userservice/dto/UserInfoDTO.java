package com.chensi.admin.userservice.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author si.chen
 */
@Data
public class UserInfoDTO implements Serializable {

    private static final long serialVersionUID = 3932297840062675104L;

    @ApiModelProperty(value = "ID")
    private String id;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "性别")
    private Integer sex;

    @ApiModelProperty(value = "省")
    private String province;

    @ApiModelProperty(value = "市")
    private String city;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "支付宝id")
    private String aliId;

    @ApiModelProperty(value = "微信id")
    private String wxId;

    @ApiModelProperty(value = "实名类型")
    private Integer realNameType;

    @ApiModelProperty(value = "实名状态")
    private Integer realNameStatus;

}
