package com.chensi.admin.userservice.dao;

import com.chensi.admin.userservice.domain.UserSuggestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author si.chen
 * @date 2019/6/1713:16
 */
@Repository
public interface UserSuggestionRepository extends JpaRepository<UserSuggestion, String>,
        JpaSpecificationExecutor<UserSuggestion> {


}
