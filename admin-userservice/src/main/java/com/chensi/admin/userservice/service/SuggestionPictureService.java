package com.chensi.admin.userservice.service;

import com.chensi.admin.userservice.domain.SuggestionPicture;
import com.chensi.admin.userservice.domain.UserSuggestion;
import com.chensi.admin.userservice.exception.BaseException;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/813:25
 */
public interface SuggestionPictureService {

    /**
     * 获取图片list
     * @param userSuggestion 用户反馈
     * @return  List<SuggestionPicture>
     * @throws BaseException 运行异常
     */
    List<SuggestionPicture> get(UserSuggestion userSuggestion) throws BaseException;
}
