package com.chensi.admin.userservice.controller;

import com.chensi.admin.userservice.dto.UserSuggestionDTO;
import com.chensi.admin.userservice.common.JsonResult;
import com.chensi.admin.userservice.controller.api.UserSuggestionApi;
import com.chensi.admin.userservice.dto.search.SearchSuggestionDTO;
import com.chensi.admin.userservice.exception.BaseException;
import com.chensi.admin.userservice.domain.UserSuggestion;
import com.chensi.admin.userservice.service.UserSuggestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author si.chen
 * @date 2019/6/1713:07
 */
@RestController
public class UserSuggestionController implements UserSuggestionApi {

    private final UserSuggestionService userSuggestionService;

    @Autowired
    public UserSuggestionController(UserSuggestionService userSuggestionService) {
        this.userSuggestionService = userSuggestionService;
    }

    @Override
    public ResponseEntity<JsonResult> save(@Valid UserSuggestion userSuggestion) throws BaseException {
        userSuggestionService.create(userSuggestion);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    public ResponseEntity<JsonResult> delete(String id) throws BaseException {
        userSuggestionService.delete(id);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    public ResponseEntity<JsonResult<Page<UserSuggestionDTO>>> page(SearchSuggestionDTO searchSuggestionDTO)
            throws BaseException {
        return ResponseEntity.ok(
                JsonResult.ok(
                        userSuggestionService.page(
                                searchSuggestionDTO.getPageNum(),
                                searchSuggestionDTO.getPageSize()
                        )
                )
        );
    }

    @Override
    public ResponseEntity<JsonResult> update(String id, UserSuggestion userSuggestion) throws BaseException {
        userSuggestionService.update(userSuggestion);
        return ResponseEntity.ok(JsonResult.ok());
    }
}
