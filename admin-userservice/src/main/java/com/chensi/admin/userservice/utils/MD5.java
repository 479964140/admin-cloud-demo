package com.chensi.admin.userservice.utils;

import org.apache.commons.codec.digest.DigestUtils;

public class MD5 {
    /**
     * MD5方法
     *
     * @param text （密码明文）
     * @param key  (用户手机号)
     * @return 密文
     * @throws Exception
     */
    public static String md5(String text, String key) {
        // 通过手机号截取字符串作为盐值
        String salt = generateSalt(key);
        // 用户密码+盐值生成MD5
        String encodeStr = DigestUtils.md5Hex(text + salt);
        return encodeStr;
    }

    /**
     * MD5验证方法
     *
     * @param text 明文
     * @param key  密钥
     * @param md5  密文
     * @return true/false
     * @throws Exception
     */
    public static boolean verify(String text, String key, String md5) {
        // 根据传入的密钥进行验证
        String md5Text = md5(text, key);
        if (md5Text.equalsIgnoreCase(md5)) {
            return true;
        }
        return false;
    }

    /**
     * @param key 用户手机号
     * @return 取用户手机号13579位+pactera作为盐值
     */
    public static String generateSalt(String key) {
        String str = "";
        str = str + key.substring(0, 1)
                + key.substring(2, 3)
                + key.substring(4, 5)
                + key.substring(6, 7)
                + key.substring(8, 9)
                + "pactera";
        return str;
    }

}
