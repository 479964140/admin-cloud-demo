package com.chensi.admin.userservice.controller.api;

import com.chensi.admin.userservice.common.Constants;
import com.chensi.admin.userservice.common.JsonResult;
import com.chensi.admin.userservice.domain.UserInfo;
import com.chensi.admin.userservice.exception.BaseException;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 用户信息接口
 *
 * @author si.chen
 * @date 2019/6/18
 */
@Api(tags = "用户服务接口文档")
@RequestMapping("user")
public interface UserInfoApi {

    /**
     * 校验手机号
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "校验手机号", notes = "客户端初次注册，先校验手机号是否注册。入参以及验证规则：手机号非空",
            response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("checkPhone")
    ResponseEntity<JsonResult> checkPhone(@Validated({Constants.CheckPhone.class}) @RequestBody UserInfo userInfo)
            throws BaseException;

    /**
     * 校验验证码
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "校验验证码", notes = "用户验证手机验证码，入参以及验证规则：手机号、验证码非空",
            response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("checkCode")
    ResponseEntity<JsonResult> checkCode(@Validated({Constants.CheckCode.class, Constants.CheckPhone.class})
                                         @RequestBody UserInfo userInfo) throws BaseException;

    /**
     * 注册
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "注册", notes = "新用户注册，入参以及验证规则：手机号，密码非空", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("register")
    ResponseEntity<JsonResult> register(@Validated({Constants.CheckPassword.class, Constants.CheckPhone.class})
                                        @RequestBody UserInfo userInfo) throws BaseException;

    /**
     * 注册--支付宝
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "注册--支付宝", notes = "新用户注册，入参以及验证规则：手机号，aliId非空", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("registerByAli")
    ResponseEntity<JsonResult> registerByAli(@Validated({Constants.CheckAli.class, Constants.CheckPhone.class})
                                             @RequestBody UserInfo userInfo) throws BaseException;

    /**
     * 注册--微信
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "注册--微信", notes = "新用户注册，入参以及验证规则：手机号，wxId非空", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("registerByWx")
    ResponseEntity<JsonResult> registerByWx(@Validated({Constants.CheckWx.class, Constants.CheckPhone.class})
                                            @RequestBody UserInfo userInfo) throws BaseException;

    /**
     * 登录
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "登录", notes = "用户登录，入参以及验证规则：手机号、密码非空", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("login")
    ResponseEntity<JsonResult> login(@Validated({Constants.CheckPassword.class, Constants.CheckPhone.class})
                                     @RequestBody UserInfo userInfo) throws BaseException;

    /**
     * 登录--支付宝
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "登录--支付宝", notes = "用户支付宝登录，入参以及验证规则：aliId非空", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("loginByAli")
    ResponseEntity<JsonResult> loginByAli(@Validated({Constants.CheckAli.class})
                                          @RequestBody UserInfo userInfo) throws BaseException;

    /**
     * 登录--微信
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "登录--微信", notes = "用户微信登录，入参以及验证规则：wxId非空", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("loginByWx")
    ResponseEntity<JsonResult> loginByWx(@Validated({Constants.CheckWx.class})
                                         @RequestBody UserInfo userInfo) throws BaseException;

    /**
     * 修改信息
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "修改信息", notes = "修改信息。入参以及验证规则：ID不为空", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "ID主键", required = true, paramType = "path", dataType = "String"),
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PutMapping("")
    ResponseEntity<JsonResult> update(@Validated({Constants.Update.class})
                                      @RequestBody UserInfo userInfo) throws BaseException;

    /**
     * 忘记密码
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "忘记密码", notes = "用户未登录找回密码，入参以及验证规则：手机号、新密码不为空",
            response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("forgotPassword")
    ResponseEntity<JsonResult> forgotPassword(@Validated({Constants.CheckPhone.class, Constants.CheckPassword.class})
                                              @RequestBody UserInfo userInfo) throws BaseException;

    /**
     * 重置密码
     *
     * @param userInfo 用户信息
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "重置密码", notes = "用户已登录重置密码，入参以及验证规则：用户ID、旧密码、新密码不为空",
            response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("resetPassword")
    ResponseEntity<JsonResult> resetPassword(@Validated({Constants.CheckId.class, Constants.CheckOldPassword.class,
            Constants.CheckPassword.class}) @RequestBody UserInfo userInfo) throws BaseException;

    /**
     * 用户注销
     *
     * @param id 用户id
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "用户注销", notes = "注销用户。入参以及验证规则：id不可为空", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID（AES加密）", required = true, paramType = "query",
                    dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "items: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @DeleteMapping("")
    ResponseEntity<JsonResult> delete(@RequestParam("id") String id) throws BaseException;

    /**
     * 详情
     *
     * @param id 用户id
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "用户详情", notes = "用户详情。入参以及验证规则：id不可为空", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID（AES加密）", required = true, paramType = "query",
                    dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "items: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @GetMapping("")
    ResponseEntity<JsonResult> get(@RequestParam("id") String id) throws BaseException;

    /**
     * 手机实名认证
     *
     * @param userInfo 用户信息
     * @return json
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "手机实名认证", notes = "手机实名认证。入参以及验证规则：姓名、身份证、手机号不可为空", response =
            JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "items: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("authenticateByPhone")
    ResponseEntity<JsonResult> authenticateByPhone(@Validated({Constants.CheckPhone.class, Constants.CheckIdCard.class,
            Constants.CheckRealName.class}) @RequestBody UserInfo userInfo) throws BaseException;

}
