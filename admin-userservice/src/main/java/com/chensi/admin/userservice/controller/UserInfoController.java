package com.chensi.admin.userservice.controller;

import com.chensi.admin.userservice.dto.UserInfoDTO;
import com.chensi.admin.userservice.common.Constants;
import com.chensi.admin.userservice.common.JsonResult;
import com.chensi.admin.userservice.controller.api.UserInfoApi;
import com.chensi.admin.userservice.domain.UserInfo;
import com.chensi.admin.userservice.exception.BaseException;
import com.chensi.admin.userservice.service.UserInfoService;
import com.chensi.admin.userservice.utils.AES;
import lombok.AllArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author si.chen
 */
@RestController
@AllArgsConstructor
public class UserInfoController implements UserInfoApi {

    private final UserInfoService userInfoService;

    @Override
    @PostMapping("checkPhone")
    public ResponseEntity<JsonResult> checkPhone(@Validated({Constants.CheckPhone.class})
                                                 @RequestBody UserInfo userInfo) throws BaseException {
        userInfoService.checkPhone(userInfo.getPhone());
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PostMapping("checkCode")
    public ResponseEntity<JsonResult> checkCode(@Validated({Constants.CheckCode.class, Constants.CheckPhone.class})
                                                @RequestBody UserInfo userInfo) throws BaseException {
        userInfoService.checkCode(userInfo.getPhone(), userInfo.getCode());
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PostMapping("register")
    public ResponseEntity<JsonResult> register(@Validated({Constants.CheckPassword.class, Constants.CheckPhone.class})
                                               @RequestBody UserInfo userInfo) throws BaseException {
        userInfoService.create(userInfo);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PostMapping("login")
    public ResponseEntity<JsonResult> login(@Validated({Constants.CheckPassword.class, Constants.CheckPhone.class})
                                            @RequestBody UserInfo userInfo) throws BaseException {
        return ResponseEntity.ok(JsonResult.ok(userInfoService.login(userInfo.getPhone(), userInfo.getPassword())));
    }

    @Override
    @PutMapping("")
    public ResponseEntity<JsonResult> update(@Validated({Constants.Update.class})
                                             @RequestBody UserInfo userInfo) throws BaseException {
        AES.setKey(Constants.SALT);
        AES.decrypt(userInfo.getId());
        userInfo.setId(AES.getDecryptedString());
        userInfo.setPassword(DigestUtils.md5Hex(userInfo.getPassword()));
        userInfoService.update(userInfo);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PostMapping("forgotPassword")
    public ResponseEntity<JsonResult> forgotPassword(
            @Validated({Constants.CheckPhone.class, Constants.CheckPassword.class})
            @RequestBody UserInfo userInfo) throws BaseException {
        userInfoService.forgotPassword(userInfo);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PostMapping("resetPassword")
    public ResponseEntity<JsonResult> resetPassword(
            @Validated({Constants.CheckId.class, Constants.CheckOldPassword.class,
                    Constants.CheckPassword.class}) @RequestBody UserInfo userInfo) throws BaseException {
        AES.setKey(Constants.SALT);
        AES.decrypt(userInfo.getId());
        userInfo.setId(AES.getDecryptedString());
        userInfoService.resetPassword(userInfo);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @DeleteMapping("")
    public ResponseEntity<JsonResult> delete(@RequestParam("id") String id) throws BaseException {
        AES.setKey(Constants.SALT);
        AES.decrypt(id);
        String userId = AES.getDecryptedString();
        userInfoService.delete(userId);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @GetMapping("")
    public ResponseEntity<JsonResult> get(@RequestParam("id") String id) throws BaseException {
        AES.setKey(Constants.SALT);
        AES.decrypt(id);
        UserInfoDTO dto = userInfoService.detail(AES.getDecryptedString());
        return ResponseEntity.ok(JsonResult.ok(dto));
    }

    @Override
    @PostMapping("authenticateByPhone")
    public ResponseEntity<JsonResult> authenticateByPhone(@Validated({Constants.CheckPhone.class,
            Constants.CheckIdCard.class, Constants.CheckRealName.class}) @RequestBody UserInfo userInfo)
            throws BaseException {
        userInfoService.authenticateByPhone(userInfo);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PostMapping("loginByAli")
    public ResponseEntity<JsonResult> loginByAli(@Validated({Constants.CheckAli.class})
                                                 @RequestBody UserInfo userInfo) throws BaseException {
        String res = userInfoService.loginByAli(userInfo.getAliId());
        return ResponseEntity.ok(JsonResult.ok(res));
    }

    @Override
    @PostMapping("loginByWx")
    public ResponseEntity<JsonResult> loginByWx(@Validated({Constants.CheckWx.class})
                                                @RequestBody UserInfo userInfo) throws BaseException {
        String res = userInfoService.loginByWx(userInfo.getWxId());
        return ResponseEntity.ok(JsonResult.ok(res));
    }

    @Override
    @PostMapping("registerByAli")
    public ResponseEntity<JsonResult> registerByAli(@Validated({Constants.CheckAli.class, Constants.CheckPhone.class})
                                                    @RequestBody UserInfo userInfo) throws BaseException {
        userInfoService.createByAliOrWx(userInfo);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PostMapping("registerByWx")
    public ResponseEntity<JsonResult> registerByWx(@Validated({Constants.CheckWx.class, Constants.CheckPhone.class})
                                                   @RequestBody UserInfo userInfo) throws BaseException {
        userInfoService.createByAliOrWx(userInfo);
        return ResponseEntity.ok(JsonResult.ok());
    }
}
