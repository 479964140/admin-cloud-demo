package com.chensi.admin.userservice.controller.api;

import com.chensi.admin.userservice.dto.UserSuggestionDTO;
import com.chensi.admin.userservice.common.Constants;
import com.chensi.admin.userservice.common.JsonResult;
import com.chensi.admin.userservice.dto.search.SearchSuggestionDTO;
import com.chensi.admin.userservice.exception.BaseException;
import com.chensi.admin.userservice.domain.UserSuggestion;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 用户意见反馈
 *
 * @author si.chen
 * @date 2019/6/18
 */
@Api(tags = "User Suggestion API")
@RequestMapping("/user/suggestion")
public interface UserSuggestionApi {

    /**
     * 创建用户意见反馈
     *
     * @param userSuggestion 用户反馈
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "创建用户意见反馈", notes = "创建用户意见反馈", response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "成功返回", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "CmsContent: name is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("")
    ResponseEntity<JsonResult> save(
            @Valid @RequestBody UserSuggestion userSuggestion) throws BaseException;

    /**
     * 删除用户意见反馈
     *
     * @param id 用户id
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "删除用户意见反馈", notes = "删除用户意见反馈。验证规则：id不可为空", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户意见反馈ID", required = true, paramType = "path",
                    dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "成功返回", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "items: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @DeleteMapping("/{id}")
    ResponseEntity<JsonResult> delete(@PathVariable("id") String id)
            throws BaseException;


    /**
     * 获取用户意见反馈列表
     *
     * @param searchSuggestionDTO 用户意见查询实体
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @GetMapping("/page")
    @ApiOperation(value = "获取用户意见反馈列表", notes = "根据用户意见反馈模库查询", response = UserSuggestionDTO.class)
    ResponseEntity<JsonResult<Page<UserSuggestionDTO>>> page(SearchSuggestionDTO searchSuggestionDTO)
            throws BaseException;

    /**
     * 更新用户意见反馈
     *
     * @param id             用户意见反馈ID
     * @param userSuggestion 用户意见实体
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "更新用户意见反馈", notes = "根据id修改用户意见反馈。验证规则：id不能为空",
            response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户意见反馈ID", required = true, paramType = "path",
                    dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "成功返回", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "CmsContent: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PutMapping("/{id}")
    ResponseEntity<JsonResult> update(@PathVariable("id") String id, @Validated({Constants.Update.class})
    @RequestBody UserSuggestion userSuggestion) throws BaseException;


}
