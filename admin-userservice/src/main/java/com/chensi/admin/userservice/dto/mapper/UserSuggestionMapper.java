package com.chensi.admin.userservice.dto.mapper;

import com.chensi.admin.userservice.dto.UserSuggestionDTO;
import com.chensi.admin.userservice.domain.UserSuggestion;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

/**
 * @author si.chen
 * @date 2019/6/1714:28
 */

@Mapper(componentModel = "spring")
public interface UserSuggestionMapper extends BaseMapper<UserSuggestion, UserSuggestionDTO> {

    /**
     * 属性更新
     *
     * @param old 原属性
     * @param nr  目标属性
     */
    void update(@MappingTarget UserSuggestion old, UserSuggestion nr);
}