package com.chensi.admin.userservice.config;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * @author si.chen
 * @date 创建时间：2019年5月27日 下午1:23:32
 */
@Configuration
public class ValidatorConfiguration {
    @Bean
    public Validator validator() {
        // true:快速校验模式    false：普通验证模式
        ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class).configure()
                .addProperty("hibernate.validator.fail_fast", "true").buildValidatorFactory();
        return validatorFactory.getValidator();

    }
}
