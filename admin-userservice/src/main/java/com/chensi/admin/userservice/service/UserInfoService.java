package com.chensi.admin.userservice.service;

import com.chensi.admin.userservice.dto.UserInfoDTO;
import com.chensi.admin.userservice.domain.UserInfo;
import com.chensi.admin.userservice.exception.BaseException;


/**
 * @author si.chen
 */
public interface UserInfoService {

    /**
     * 检验手机号是否注册
     *
     * @param phone 手机号
     * @throws BaseException 基本异常
     */
    void checkPhone(String phone) throws BaseException;

    /**
     * 检验验证码和手机号匹配
     *
     * @param phone 手机号
     * @param code  验证码
     * @throws BaseException 基本异常
     */
    void checkCode(String phone, String code) throws BaseException;

    /**
     * 登录
     *
     * @param phone    手机号
     * @param password 密码
     * @return 加密后的id
     * @throws BaseException 基本异常
     */
    String login(String phone, String password) throws BaseException;

    /**
     * 登录--支付宝
     *
     * @param aliId 支付宝id
     * @return 加密后的id
     * @throws BaseException 基本异常
     */
    String loginByAli(String aliId) throws BaseException;

    /**
     * 登录--微信
     *
     * @param wxId 微信id
     * @return 加密后的id
     * @throws BaseException 基本异常
     */
    String loginByWx(String wxId) throws BaseException;

    /**
     * 未登录的时候忘记密码
     *
     * @param userInfo 用户信息
     * @throws BaseException 基本异常
     */
    void forgotPassword(UserInfo userInfo) throws BaseException;

    /**
     * 用户动态查询
     *
     * @param userInfo 用户信息
     * @return 用户信息
     * @throws BaseException 基本异常
     */
    UserInfo find(UserInfo userInfo) throws BaseException;

    /**
     * 按照手机查询
     *
     * @param phone 手机号
     * @return 用户信息
     * @throws BaseException 基本异常
     */
    UserInfo findByPhone(String phone) throws BaseException;

    /**
     * 获取信息
     *
     * @param id 用户id
     * @return 用户信息
     * @throws BaseException 基本异常
     */
    UserInfo get(String id) throws BaseException;

    /**
     * 用户详情
     *
     * @param id 用户id
     * @return 用户信息DTO
     * @throws BaseException 基本异常
     */
    UserInfoDTO detail(String id) throws BaseException;

    /**
     * 按照手机号和密码查询
     *
     * @param phone    手机号
     * @param password 密码
     * @return 用户信息
     * @throws BaseException 基本异常
     */
    UserInfo findByPhoneAndPassword(String phone, String password) throws BaseException;

    /**
     * 新建用户
     *
     * @param userInfo 用户信息
     * @throws BaseException 基本异常
     */
    void create(UserInfo userInfo) throws BaseException;

    /**
     * 新建用户--第三方渠道
     *
     * @param userInfo 用户信息
     * @throws BaseException 基本异常
     */
    void createByAliOrWx(UserInfo userInfo) throws BaseException;

    /**
     * 用户更新
     *
     * @param userInfo 用户信息
     * @return 用户信息
     * @throws BaseException 基本异常
     */
    UserInfo update(UserInfo userInfo) throws BaseException;

    /**
     * 用户删除
     *
     * @param id 用户id
     * @throws BaseException 基本异常
     */
    void delete(String id) throws BaseException;

    /**
     * 登录后的重置密码
     *
     * @param userInfo 用户信息
     * @throws BaseException 基本异常
     */
    void resetPassword(UserInfo userInfo) throws BaseException;

    /**
     * 手机实名认证
     *
     * @param userInfo 用户信息
     * @throws BaseException 基本异常
     */
    void authenticateByPhone(UserInfo userInfo) throws BaseException;

}
