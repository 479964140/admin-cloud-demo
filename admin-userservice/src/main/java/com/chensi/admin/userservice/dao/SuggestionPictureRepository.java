package com.chensi.admin.userservice.dao;

import com.chensi.admin.userservice.domain.SuggestionPicture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author si.chen
 * @date 2019/7/810:35
 */
public interface SuggestionPictureRepository extends JpaRepository<SuggestionPicture, String>,
        JpaSpecificationExecutor<SuggestionPicture> {

}
