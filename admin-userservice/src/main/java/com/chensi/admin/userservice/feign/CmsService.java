package com.chensi.admin.userservice.feign;

import com.chensi.admin.userservice.common.Constants;
import com.chensi.admin.userservice.common.JsonResult;
import com.chensi.admin.userservice.domain.CmsActivityApply;
import com.chensi.admin.userservice.dto.CmsActivityDTO;
import com.chensi.admin.userservice.exception.BaseException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/6/24 14:49
 */

@FeignClient(value = "CMS", path = Constants.CMS_PATH)
public interface CmsService {

    /**
     * 获取所有活动
     *
     * @return 活动集合
     */
    @GetMapping(value = "/activity")
    ResponseEntity<JsonResult<List<CmsActivityDTO>>> list();

    /**
     * 活动-用户报名
     *
     * @param cmsActivityApply 活动id，报名人员id
     * @return 报名状态
     * @throws BaseException 报名异常
     */
    @PostMapping("/activity/apply")
    ResponseEntity<JsonResult> save(CmsActivityApply cmsActivityApply) throws BaseException;
}
