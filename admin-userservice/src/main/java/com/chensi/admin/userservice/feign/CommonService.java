package com.chensi.admin.userservice.feign;

import com.chensi.admin.userservice.common.Constants;
import com.chensi.admin.userservice.common.JsonResult;
import com.chensi.admin.userservice.domain.UserInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author si.chen
 * @date 2019/6/2410:59
 */
@FeignClient(value = "COMMON", path = Constants.COMMON_PATH)
public interface CommonService {

    /**
     * 返回天气
     *
     * @return 消息体
     */
    @PostMapping(value = "/juhe/weatherForecast")
    ResponseEntity<Object> getWeather();

    /**
     * 手机实名认证
     *
     * @param userInfo 用户信息
     * @return 消息体
     */
    @PostMapping(value = "/juhe/tripleNetworkAuthentication")
    ResponseEntity<JsonResult> authenticateByPhone(UserInfo userInfo);

}
