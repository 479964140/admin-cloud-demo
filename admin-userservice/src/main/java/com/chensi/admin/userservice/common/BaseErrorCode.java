package com.chensi.admin.userservice.common;

import lombok.Getter;

/**
 * 错误码和信息
 *
 * @author si.chen
 * @date 2019/6/18
 */
@Getter
public enum BaseErrorCode {

    /**
     * Base错误码和信息
     */
    INVALID_PARAMETERS("00001", "Invalid Parameters"),
    ITEM_NOT_FOUND("00002", "Item Not Found"),
    ITEM_REPEAT("00003", "Item Repeat"),
    STATUS_NOT_MATCH("00004", "status not match"),
    ITEM_IS_USED("00005", "Item is used"),
    ITEM_NOT_MATCH("00006", "Item not matched"),
    /**
     * 用户模块错误码和信息
     */
    MISS_PARAMETERS("10001", "参数不能为空"),
    UNKNOWN_EXCEPTION("10002", "服务器忙，请等待..."),
    CODE_ERROR("10003", "您输入的验证码错误请重新获取"),
    PHONE_NOT_EXIST("10004", "您输入的手机号未注册"),
    PASSWORD_ERROR("10005", "您输入的手机号/密码有误"),
    LOGIN_FAIL_TOO_MUCH("10006", "输入错误次数超过上限"),
    USER_NOT_EXIST("10007", "不存在的用户"),
    OLD_PASSWORD_ERROR("10008", "您输入的旧密码有误"),
    PHONE_EXIST("10009", "您输入的手机号已注册"),
    PHONE_FORMAT_ERROR("10010", "密码要同时含有数字和字母，且长度要在8-16位之间"),
    FILE_NOT_EXIST("10011", "文件不存在"),
    UPLOAD_ERROR("10012", "上传文件失败"),
    REAL_NAME_ERROR("10013", "实名认证不匹配"),
    AlI_WX_NOT_EXIST("10014", "未注册的支付宝或者微信账户"),
    PHONE_NOT_BIND("10015", "未绑定手机号"),
    PASSWORD_BLANK("10016", "未初始化密码"),
    ;


    private String code;
    private String message;

    BaseErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
