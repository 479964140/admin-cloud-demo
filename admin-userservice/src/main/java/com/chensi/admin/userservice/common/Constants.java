package com.chensi.admin.userservice.common;

/**
 * 常量
 *
 * @author si.chen
 * @date 2019/6/18
 */
public class Constants {
    /**
     * 删除标志： 否
     */
    public static final Integer DELETED_NO = 0;
    /**
     * 删除标志： 是
     */
    public static final Integer DELETED_YES = 1;

    public static final Integer PAGE_NUM = 0;
    public static final Integer PAGE_SIZE = 10;

    /**
     * 登录失败次数上限
     */
    public static final Integer LOGIN_ERROR_LIMIT = 5;

    /**
     * 登录失败时间范围（秒）：单位时间内的登录失败
     */
    public static final Integer LOGIN_ERROR_TIME_SCOPE = 30 * 60;


    /**
     * 加盐
     */
    public static final String SALT = "chensi";

    public static final String COMMON_PATH = "/common";

    /**
     * http状态
     */
    public static final Integer HTTP_CODE = 200;
    public static final String HTTP_SUCCESS = "success";
    public static final String CODE = "0";


    public static final String CMS_PATH = "/cms";

    public static final String code = "code";
    public static final String data = "data";

    //==================================================================
    /**
     * 实名认证匹配
     */
    public static final String REAL_NAME_YES = "1";

    /**
     * 实名认证返回结果集
     */
    public static final String REAL_NAME_RESULT = "result";
    public static final String REAL_NAME_RES = "res";
    public static final String REAL_NAME_REALNAME = "realname";
    public static final String REAL_NAME_IDCARD = "idcard";
    public static final String REAL_NAME_PROVINCE = "province";
    public static final String REAL_NAME_CITY = "city";


    /**
     * 实名认证状态：未认证
     */
    public static final Integer REAL_NAME_STATUS_NO = 0;

    /**
     * 实名认证状态：已认证
     */
    public static final Integer REAL_NAME_STATUS_YES = 1;

    /**
     * 实名认证类型：支付宝
     */
    public static final Integer REAL_NAME_ALI = 1;
    /**
     * 实名认证类型：三网手机实名
     */
    public static final Integer REAL_NAME_PHONE = 2;
    /**
     * 实名认证类型：手持照片
     */
    public static final Integer REAL_NAME_PHOTO = 3;

    //=============================================================

    /**
     * hibernate validator groups
     */
    public interface Update {
    }

    public interface CheckPhone {
    }

    public interface CheckCode {
    }

    public interface CheckPassword {
    }

    public interface CheckOldPassword {
    }

    public interface CheckRealName {
    }

    public interface CheckIdCard {
    }

    public interface CheckId {
    }

    public interface CheckAli {
    }

    public interface CheckWx {
    }

}