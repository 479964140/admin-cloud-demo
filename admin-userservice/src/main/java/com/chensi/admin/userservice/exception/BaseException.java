package com.chensi.admin.userservice.exception;

import com.chensi.admin.userservice.common.BaseErrorCode;
import lombok.Getter;

/**
 * 基本异常
 *
 * @author si.chen
 * @date 2019/6/24 15:09
 */
@Getter
public class BaseException extends Exception {
    private static final long serialVersionUID = -7500006151235472097L;
    private String errorCode;
    private String errorMessage;

    public BaseException(String errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public BaseException(BaseErrorCode baseErrorCode) {
        super(baseErrorCode.getMessage());
        this.errorCode = baseErrorCode.getCode();
        this.errorMessage = baseErrorCode.getMessage();
    }


    public BaseException(String errorCode, String errorMessage, String desc) {
        super(desc);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}
