package com.chensi.admin.userservice.dto.search;

import lombok.Data;

import java.io.Serializable;

/**
 * @author si.chen
 */

@Data
public class SearchPageDTO implements Serializable {

    private static final long serialVersionUID = 691378303636759502L;

    private Integer pageNum;

    private Integer pageSize;
}
