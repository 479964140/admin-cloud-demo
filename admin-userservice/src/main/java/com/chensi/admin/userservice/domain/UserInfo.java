package com.chensi.admin.userservice.domain;

import com.chensi.admin.userservice.common.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author si.chen
 */
@Getter
@Setter
@Entity(name = "user_info")
@Where(clause = "deleted = 0")
public class UserInfo extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -8483247022053688937L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    @ApiModelProperty(value = "ID")
    private String id;

    @NotBlank(message = "真实姓名不为空", groups = {Constants.CheckRealName.class})
    @ApiModelProperty(value = "真实姓名")
    @Column(name = "real_name")
    private String realName;

    @ApiModelProperty(value = "性别")
    @Column(name = "sex")
    private Integer sex;

    @ApiModelProperty(value = "省")
    @Column(name = "province")
    private String province;

    @ApiModelProperty(value = "市")
    @Column(name = "city")
    private String city;

    @NotBlank(message = "支付宝id不为空", groups = {Constants.CheckAli.class})
    @ApiModelProperty(value = "支付宝id")
    @Column(name = "ali_id")
    private String aliId;

    @NotBlank(message = "微信id不为空", groups = {Constants.CheckWx.class})
    @ApiModelProperty(value = "微信id")
    @Column(name = "wx_id")
    private String wxId;

    @NotBlank(message = "手机号不为空", groups = {Constants.CheckPhone.class})
    @ApiModelProperty(value = "手机号")
    @Column(name = "phone")
    private String phone;

    @NotBlank(message = "密码不为空", groups = {Constants.CheckPassword.class})
    @ApiModelProperty(value = "密码")
    @Column(name = "password")
    private String password;

    @NotBlank(message = "身份证号不为空", groups = {Constants.CheckIdCard.class})
    @Column(name = "id_card_num")
    @ApiModelProperty(value = "身份证号")
    private String idCardNum;

    @Column(name = "real_name_type")
    @ApiModelProperty(value = "实名类型")
    private Integer realNameType;

    @Column(name = "real_name_status")
    @ApiModelProperty(value = "实名状态")
    private Integer realNameStatus;

    @Column(name = "avatar")
    @ApiModelProperty(value = "头像")
    private String avatar;

    //---------------------------------------------

    @Transient
    @NotBlank(message = "验证码不为空", groups = {Constants.CheckCode.class})
    @ApiModelProperty(value = "验证码")
    private String code;

    @Transient
    @NotBlank(message = "旧密码不为空", groups = {Constants.CheckOldPassword.class})
    @ApiModelProperty(value = "旧密码")
    private String oldPassword;

    @Transient
    private String idNe;

}
