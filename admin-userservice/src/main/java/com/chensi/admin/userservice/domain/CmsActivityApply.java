package com.chensi.admin.userservice.domain;

import com.chensi.admin.userservice.common.Constants;
import com.chensi.admin.userservice.utils.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author si.chen
 * @date 2019/6/17 14:58
 */

@Getter
@Setter
@Entity(name = "cms_activity_apply")
public class CmsActivityApply implements Serializable {

    private static final long serialVersionUID = 1474108460040911456L;
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "id required", groups = Constants.Update.class)
    private String id;

    @Column(name = "cms_activity_id")
    @NotBlank(message = "活动id不能为空!")
    private String cmsActivityId;

    @Column(name = "app_user_id")
    @NotBlank(message = "报名人员不能为空!")
    private String appUserId;

    @CreatedDate
    @Column(name = "create_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @LastModifiedDate
    @Column(name = "update_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime updateTime;
}
