package com.chensi.admin.userservice.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.chensi.admin.userservice.common.Constants;
import com.chensi.admin.userservice.utils.DateUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * @author si.chen
 * @date 2019/6/18
 */
@MappedSuperclass
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity {

    @Column(name = "deleted")
    private Integer deleted = Constants.DELETED_NO;

    @CreatedDate
    @Column(name = "create_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private Date createTime;

    @LastModifiedDate
    @Column(name = "update_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private Date updateTime;
}
