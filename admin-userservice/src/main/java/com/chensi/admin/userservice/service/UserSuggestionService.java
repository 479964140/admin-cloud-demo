package com.chensi.admin.userservice.service;

import com.chensi.admin.userservice.dto.UserSuggestionDTO;
import com.chensi.admin.userservice.domain.UserSuggestion;
import com.chensi.admin.userservice.exception.BaseException;
import org.springframework.data.domain.Page;

/**
 * @author si.chen
 * @date 2019/6/1713:09
 */
public interface UserSuggestionService {

    /**
     * 新建用户意见
     *
     * @param userSuggestion 用户意见
     * @throws BaseException 基本异常
     */
    void create(UserSuggestion userSuggestion) throws BaseException;

    /**
     * 删除用户意见
     *
     * @param id 用户意见id
     * @throws BaseException 基本异常
     */
    void delete(String id) throws BaseException;

    /**
     * 获取用户意见
     *
     * @param id 用户意见id
     * @return 用户意见
     * @throws BaseException 基本异常
     */
    UserSuggestion get(String id) throws BaseException;

    /**
     * 更新用户意见
     *
     * @param userSuggestion 用户意见
     * @return 用户意见
     * @throws BaseException 基本异常
     */
    UserSuggestion update(UserSuggestion userSuggestion) throws BaseException;

    /**
     * 分页查询用户意见
     *
     * @param pageNum  页号
     * @param pageSize 页大小
     * @return 分页的用户意见
     * @throws BaseException 基本异常
     */
    Page<UserSuggestionDTO> page(Integer pageNum, Integer pageSize) throws BaseException;

}
