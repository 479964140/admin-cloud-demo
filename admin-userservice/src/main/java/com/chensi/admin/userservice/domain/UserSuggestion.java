package com.chensi.admin.userservice.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.chensi.admin.userservice.common.Constants;
import com.chensi.admin.userservice.utils.DateUtil;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * @author si.chen
 * @date 2019/6/1417:33
 */

@Getter
@Setter
@Entity(name = "user_suggestion")
@Where(clause = "deleted = 0")
@EntityListeners(AuditingEntityListener.class)
public class UserSuggestion implements Serializable {

    private static final long serialVersionUID = -6558320402449549398L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    private String id;

    @NotBlank(message = "反馈内容不能为空")
    @Column(name = "content")
    private String content;

    @Column(name = "operate_by")
    private String operateBy;

    @Column(name = "create_by")
    private String createBy;

    @LastModifiedDate
    @Column(name = "operate_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime operateTime;

    @CreatedDate
    @Column(name = "create_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private LocalDateTime createTime;

    @Column(name = "deleted")
    private Integer deleted = 0;

    @OneToMany(mappedBy="userSuggestion")
    private Set<SuggestionPicture> SuggestionPictures = new HashSet<>();
}
