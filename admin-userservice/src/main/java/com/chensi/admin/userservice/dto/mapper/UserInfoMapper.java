package com.chensi.admin.userservice.dto.mapper;

import com.chensi.admin.userservice.dto.UserInfoDTO;
import com.chensi.admin.userservice.domain.UserInfo;
import org.mapstruct.Mapper;

/**
 * @author si.chen
 */
@Mapper(componentModel = "spring")
public interface UserInfoMapper extends BaseMapper<UserInfo, UserInfoDTO>{

}
