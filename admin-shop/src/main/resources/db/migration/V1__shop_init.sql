
/*==============================================================*/
/* Table: company                                               */
/*==============================================================*/
create table company (
   id                   VARCHAR(32)          not null,
   name                 VARCHAR(32)          null,
   title                VARCHAR(32)          null,
   pic                  VARCHAR(2000)        null,
   fee_scope            VARCHAR(32)          null,
   tel                  VARCHAR(32)          null,
   business_hour        VARCHAR(32)          null,
   address              VARCHAR(100)         null,
   px                   VARCHAR(32)          null,
   py                   VARCHAR(32)          null,
   service_scope        VARCHAR(255)         null,
   service_type         VARCHAR(255)         null,
   service_detail       VARCHAR(2000)        null,
   is_deleted           INT2                 null,
   constraint PK_COMPANY primary key (id)
);

/*==============================================================*/
/* Table: company_service                                       */
/*==============================================================*/
create table company_service (
   id                   VARCHAR(32)          not null,
   company_id           VARCHAR(32)          null,
   name                 VARCHAR(100)         null,
   detail               VARCHAR(255)         null,
   constraint PK_COMPANY_SERVICE primary key (id)
);

/*==============================================================*/
/* Table: device_type                                           */
/*==============================================================*/
create table device_type (
   id                   VARCHAR(32)          not null,
   name                 VARCHAR(32)          null,
   sort                 INT4                 null,
   is_deleted           INT2                 null,
   constraint PK_DEVICE_TYPE primary key (id)
);

/*==============================================================*/
/* Table: goods                                                 */
/*==============================================================*/
create table goods (
   id                   VARCHAR(32)          not null,
   name                 VARCHAR(100)         null,
   thumb                VARCHAR(255)         null,
   price                NUMERIC(11,2)        null,
   price_max            NUMERIC(11,2)        null,
   price_min            NUMERIC(11,2)        null,
   unit                 VARCHAR(32)          null,
   goods_type_id        VARCHAR(32)          null,
   company_id           VARCHAR(32)          null,
   is_recommended       INT2                 null,
   sort                 INT4                 null,
   create_time          TIMESTAMP            null,
   update_time          TIMESTAMP            null,
   is_deleted           INT2                 null,
   constraint PK_GOODS primary key (id)
);

comment on column goods.thumb is
'缩略图';

comment on column goods.unit is
'计量单位';

comment on column goods.goods_type_id is
'商品类型';

comment on column goods.is_recommended is
'是否推荐';

/*==============================================================*/
/* Table: goods_pic                                             */
/*==============================================================*/
create table goods_pic (
   id                   VARCHAR(32)          not null,
   goods_id             VARCHAR(32)          null,
   pic                  VARCHAR(2000)        null,
   sort                 INT4                 null,
   constraint PK_GOODS_PIC primary key (id)
);

/*==============================================================*/
/* Table: goods_type                                            */
/*==============================================================*/
create table goods_type (
   id                   VARCHAR(32)          not null,
   name                 VARCHAR(32)          null,
   parent_id            VARCHAR(32)          null,
   level                INT2                 null,
   sort                 INT4                 null,
   is_deleted           INT2                 null,
   constraint PK_GOODS_TYPE primary key (id)
);

/*==============================================================*/
/* Table: goods_unit                                            */
/*==============================================================*/
create table goods_unit (
   id                   VARCHAR(32)          not null,
   name                 VARCHAR(32)          null,
   constraint PK_GOODS_UNIT primary key (id)
);

/*==============================================================*/
/* Table: goods_user                                            */
/*==============================================================*/
create table goods_user (
   id                   VARCHAR(32)          not null,
   user_id              VARCHAR(32)          null,
   goods_id             VARCHAR(32)          null,
   constraint PK_GOODS_USER primary key (id)
);

/*==============================================================*/
/* Table: navigation                                            */
/*==============================================================*/
create table navigation (
   id                   VARCHAR(32)          not null,
   pic                  VARCHAR(2000)        null,
   sort                 INT4                 null,
   constraint PK_NAVIGATION primary key (id)
);
