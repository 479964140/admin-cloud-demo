package com.chensi.admin.shop.common;

/**
 * 常量
 *
 * @author si.chen
 * @date 2019/6/18
 */
public class Constants {
    /**
     * 否
     */
    public static final Integer NO = 0;
    /**
     * 是
     */
    public static final Integer YES = 1;

    public static final Integer PAGE_NUM = 0;
    public static final Integer PAGE_SIZE = 10;

    /**
     * 登录失败次数上限
     */
    public static final Integer LOGIN_ERROR_LIMIT = 5;

    /**
     * 登录失败时间范围（秒）：单位时间内的登录失败
     */
    public static final Integer LOGIN_ERROR_TIME_SCOPE = 30 * 60;


    /**
     * 加盐
     */
    public static final String SALT = "MyFuNing";

    public static final String COMMON_PATH = "/common";

    /**
     * http状态
     */
    public static final Integer HTTP_CODE = 200;
    public static final String HTTP_SUCCESS = "success";
    public static final String CODE = "0";


    public static final String CMS_PATH = "/cms";

    public static final String code = "code";
    public static final String data = "data";

    //==================================================================


    //=============================================================

    /**
     * hibernate validator groups
     */
    public interface CheckId {
    }


}