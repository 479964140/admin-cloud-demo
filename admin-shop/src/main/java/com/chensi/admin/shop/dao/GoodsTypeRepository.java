package com.chensi.admin.shop.dao;

import com.chensi.admin.shop.domain.GoodsType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author si.chen
 * @date 2019/7/12 8:49
 */
@Repository
public interface GoodsTypeRepository extends JpaRepository<GoodsType, String>, JpaSpecificationExecutor<GoodsType> {
}
