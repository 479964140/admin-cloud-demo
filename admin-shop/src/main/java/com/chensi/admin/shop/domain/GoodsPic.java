package com.chensi.admin.shop.domain;

import com.chensi.admin.shop.common.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 商品--图片
 *
 * @author si.chen
 * @date 2019/7/8 16:03
 */
@Setter
@Getter
@Entity(name = "goods_pic")
public class GoodsPic extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -4741421095856436164L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "商品id")
    @JoinColumn(name = "goods_id")
    @ManyToOne
    private Goods goods;

    @ApiModelProperty(value = "图片")
    @Column(name = "pic")
    private String pic;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

}
