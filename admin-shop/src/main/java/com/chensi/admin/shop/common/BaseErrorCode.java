package com.chensi.admin.shop.common;

import lombok.Getter;

/**
 * 错误码和信息
 *
 * @author si.chen
 * @date 2019/6/18
 */
@Getter
public enum BaseErrorCode {

    /**
     * Base错误码和信息
     */
    INVALID_PARAMETERS("00001", "无效的参数"),
    ITEM_NOT_FOUND("00002", "对象不存在"),
    ITEM_REPEAT("00003", "Item Repeat"),
    STATUS_NOT_MATCH("00004", "status not match"),
    ITEM_IS_USED("00005", "对象已存在"),
    ITEM_NOT_MATCH("00006", "Item not matched"),
    MISS_PARAMETERS("00007", "参数不能为空"),
    UNKNOWN_EXCEPTION("00008", "服务器忙，请等待..."),
    FILE_NOT_EXIST("00009", "文件不存在"),
    UPLOAD_ERROR("00010", "上传文件失败"),
    NAME_IS_USED("00011", "名称已存在"),
    NOT_DELETE_IN_USE("00012", "存在已关联的对象，禁止删除"),
    IS_USED("00013", "相关内容已存在"),
    ;


    private String code;
    private String message;

    BaseErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
