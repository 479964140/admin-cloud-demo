package com.chensi.admin.shop.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author si.chen
 * @date 2019/7/15 15:26
 */
@Data
public class GoodsPicDTO implements Serializable {

    private static final long serialVersionUID = -3227690709294266035L;

    private String pic;

    private String sort;

}
