package com.chensi.admin.shop.service;

import com.chensi.admin.shop.domain.GoodsType;
import com.chensi.admin.shop.exception.BaseException;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/12 8:53
 */
public interface GoodsTypeService {
    /**
     * 新建
     *
     * @param goodsType 商品类型
     * @throws BaseException 基本异常
     */
    void create(GoodsType goodsType) throws BaseException;

    /**
     * 修改
     *
     * @param goodsType 商品类型
     * @throws BaseException 基本异常
     */
    void update(GoodsType goodsType) throws BaseException;

    /**
     * 删除
     *
     * @param id id
     * @throws BaseException 基本异常
     */
    void delete(String id) throws BaseException;

    /**
     * 查询多个
     *
     * @param goodsType 商品类型
     * @return 商品类型
     * @throws BaseException 基本异常
     */
    List<GoodsType> list(GoodsType goodsType) throws BaseException;
}
