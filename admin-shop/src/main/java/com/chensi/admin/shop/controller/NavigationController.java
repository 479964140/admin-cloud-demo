package com.chensi.admin.shop.controller;

import com.chensi.admin.shop.common.Constants;
import com.chensi.admin.shop.common.JsonResult;
import com.chensi.admin.shop.domain.Navigation;
import com.chensi.admin.shop.exception.BaseException;
import com.chensi.admin.shop.service.NavigationService;
import com.chensi.admin.shop.controller.api.NavigationApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/19 10:22
 */
@RestController
@RequestMapping("navigation")
public class NavigationController implements NavigationApi {

    private final NavigationService navigationService;

    @Autowired
    public NavigationController(NavigationService navigationService) {
        this.navigationService = navigationService;
    }

    @Override
    @PostMapping("list")
    public ResponseEntity<JsonResult> list() throws BaseException {
        List<Navigation> list = navigationService.list();
        return ResponseEntity.ok(JsonResult.ok(list));
    }

    @Override
    @PostMapping("")
    public ResponseEntity<JsonResult> create(@Validated @RequestBody Navigation navigation) throws BaseException {
        navigationService.create(navigation);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PutMapping("")
    public ResponseEntity<JsonResult> update(@Validated({Constants.CheckId.class})
                                             @RequestBody Navigation navigation) throws BaseException {
        navigationService.update(navigation);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @DeleteMapping("{id}")
    public ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException {
        navigationService.delete(id);
        return ResponseEntity.ok(JsonResult.ok());
    }
}
