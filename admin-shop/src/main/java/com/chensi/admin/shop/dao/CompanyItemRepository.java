package com.chensi.admin.shop.dao;

import com.chensi.admin.shop.domain.CompanyItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author si.chen
 * @date 2019/7/19 8:57
 */
@Repository
public interface CompanyItemRepository extends JpaRepository<CompanyItem, String>, JpaSpecificationExecutor<CompanyItem> {
}
