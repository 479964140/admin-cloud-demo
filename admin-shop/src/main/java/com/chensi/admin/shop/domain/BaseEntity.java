package com.chensi.admin.shop.domain;

import com.chensi.admin.shop.common.PageParam;
import com.chensi.admin.shop.common.SortParam;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author si.chen
 * @date 2019/7/11 17:31
 */
@Setter
@Getter
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = -7984767813715983544L;

    @Transient
    private String idNe;

    @Transient
    private String nameLike;

    @Transient
    private PageParam pageParam;

    @Transient
    private SortParam sortParam;
}
