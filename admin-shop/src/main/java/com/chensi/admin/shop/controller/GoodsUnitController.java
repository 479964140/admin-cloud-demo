package com.chensi.admin.shop.controller;

import com.chensi.admin.shop.common.Constants;
import com.chensi.admin.shop.common.JsonResult;
import com.chensi.admin.shop.domain.GoodsUnit;
import com.chensi.admin.shop.service.GoodsUnitService;
import com.chensi.admin.shop.controller.api.GoodsUnitApi;
import com.chensi.admin.shop.exception.BaseException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/9 9:33
 */
@Log4j2
@RestController
@RequestMapping("goodsUnit")
public class GoodsUnitController implements GoodsUnitApi {

    private final GoodsUnitService goodsUnitService;

    @Autowired
    public GoodsUnitController(GoodsUnitService goodsUnitService) {
        this.goodsUnitService = goodsUnitService;
    }

    @Override
    @PostMapping("list")
    public ResponseEntity<JsonResult> list(@RequestBody GoodsUnit goodsUnit) throws BaseException {
        List<GoodsUnit> list = goodsUnitService.list(goodsUnit);
        return ResponseEntity.ok(JsonResult.ok(list));
    }

    @Override
    @PostMapping("")
    public ResponseEntity<JsonResult> create(@Validated @RequestBody GoodsUnit goodsUnit) throws BaseException {
        goodsUnitService.create(goodsUnit);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PutMapping("")
    public ResponseEntity<JsonResult> update(@Validated({Constants.CheckId.class}) @RequestBody GoodsUnit goodsUnit)
            throws BaseException {
        goodsUnitService.update(goodsUnit);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @DeleteMapping("{id}")
    public ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException {
        goodsUnitService.delete(id);
        return ResponseEntity.ok(JsonResult.ok());
    }
}
