package com.chensi.admin.shop.dto.mapper;

import com.chensi.admin.shop.domain.CompanyItem;
import com.chensi.admin.shop.dto.CompanyItemDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author si.chen
 * @date 2019/7/22 16:05
 */
@Mapper(componentModel = "spring")
public interface CompanyItemMapper extends BaseMapper<CompanyItem, CompanyItemDTO> {

    @Mappings({
            @Mapping(source = "company.id", target = "companyId"),
            @Mapping(source = "company.name", target = "companyName"),
    })
    @Override
    CompanyItemDTO toDTO(CompanyItem companyItem);
}
