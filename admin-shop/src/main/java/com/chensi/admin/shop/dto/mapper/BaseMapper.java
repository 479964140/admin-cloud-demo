package com.chensi.admin.shop.dto.mapper;

/**
 * @author si.chen
 */
public interface BaseMapper<E, D> {
    /**
     * DTO转实体
     *
     * @param d DTO
     * @return 实体
     */
    E toEntity(D d);

    /**
     * 实体转DTO
     *
     * @param e 实体
     * @return DTO
     */
    D toDTO(E e);
}
