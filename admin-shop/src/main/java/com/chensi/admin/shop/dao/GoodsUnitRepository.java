package com.chensi.admin.shop.dao;

import com.chensi.admin.shop.domain.GoodsUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author si.chen
 * @date 2019/7/15 17:05
 */
@Repository
public interface GoodsUnitRepository extends JpaRepository<GoodsUnit, String>, JpaSpecificationExecutor<GoodsUnit> {
}
