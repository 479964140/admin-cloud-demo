package com.chensi.admin.shop.dto.mapper;

import com.chensi.admin.shop.domain.Goods;
import com.chensi.admin.shop.dto.GoodsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author si.chen
 * @date 2019/7/9 15:16
 */
@Mapper(componentModel = "spring")
public interface GoodsMapper extends BaseMapper<Goods, GoodsDTO> {

    @Override
    @Mappings({
            @Mapping(source = "goodsUnit.id", target = "unitId"),
            @Mapping(source = "goodsUnit.name", target = "unitName"),
            @Mapping(source = "goodsType.id", target = "goodsTypeId"),
            @Mapping(source = "goodsType.name", target = "goodsTypeName"),
            @Mapping(source = "company.id", target = "companyId"),
            @Mapping(source = "company.name", target = "companyName"),
    })
    GoodsDTO toDTO(Goods goods);
}
