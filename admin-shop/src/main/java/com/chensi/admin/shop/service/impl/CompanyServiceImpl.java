package com.chensi.admin.shop.service.impl;

import com.chensi.admin.shop.common.BaseErrorCode;
import com.chensi.admin.shop.common.Constants;
import com.chensi.admin.shop.domain.Company;
import com.chensi.admin.shop.domain.CompanyItem;
import com.chensi.admin.shop.domain.Goods;
import com.chensi.admin.shop.dto.GoodsDTO;
import com.chensi.admin.shop.service.CompanyItemService;
import com.chensi.admin.shop.service.GoodsService;
import com.chensi.admin.shop.util.BeanUtils;
import com.chensi.admin.shop.util.CommonUtil;
import com.chensi.admin.shop.util.SpecificationUtil;
import com.chensi.admin.shop.dao.CompanyRepository;
import com.chensi.admin.shop.dto.CompanyItemDTO;
import com.chensi.admin.shop.exception.BaseException;
import com.chensi.admin.shop.service.CompanyService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author si.chen
 * @date 2019/7/19 8:59
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;

    private final GoodsService goodsService;

    private final CompanyItemService companyItemService;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository, GoodsService goodsService, CompanyItemService companyItemService) {
        this.companyRepository = companyRepository;
        this.goodsService = goodsService;
        this.companyItemService = companyItemService;
    }

    @Override
    public Company get(String id) throws BaseException {
        Optional<Company> optional = companyRepository.findById(id);
        return optional.orElseThrow(() -> new BaseException(BaseErrorCode.ITEM_NOT_FOUND));
    }

    @Override
    public List<Company> list(Company entity) throws BaseException {
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        return companyRepository.findAll(this.getSpecification(entity), sort);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(Company entity) throws BaseException {
        this.checkUniqueForCreate(entity);
        companyRepository.save(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Company company) throws BaseException {
        Company entity = this.get(company.getId());
        BeanUtils.copyNotNullProperties(company, entity);
        this.checkUniqueForUpdate(entity);
        companyRepository.save(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String id) throws BaseException {
        //判断商品
        Goods param = new Goods();
        Company company = new Company();
        company.setId(id);
        param.setCompany(company);
        List<GoodsDTO> list = goodsService.list(param);
        if (CommonUtil.isNotEmptyList(list)) {
            throw new BaseException(BaseErrorCode.NOT_DELETE_IN_USE);
        } else {
            Company entity = this.get(id);
            entity.setDeleted(Constants.YES);
            companyRepository.save(entity);
            CompanyItem companyItem = new CompanyItem();
            companyItem.setCompany(company);
            List<CompanyItemDTO> companyItemList = companyItemService.list(companyItem);
            if (CommonUtil.isNotEmptyList(companyItemList)) {
                for (CompanyItemDTO item : companyItemList) {
                    companyItemService.delete(item.getId());
                }
            }
        }
    }

    private void checkUniqueForCreate(Company entity) throws BaseException {
        if (StringUtils.isNotBlank(entity.getName())) {
            Company param = new Company();
            param.setName(entity.getName());
            if (CommonUtil.isNotEmptyList(this.list(param))) {
                throw new BaseException(BaseErrorCode.NAME_IS_USED);
            }
        }
    }

    @SuppressWarnings("Duplicates")
    private void checkUniqueForUpdate(Company entity) throws BaseException {
        if (StringUtils.isNotBlank(entity.getName())) {
            Company param = new Company();
            param.setName(entity.getName().trim());
            param.setIdNe(entity.getId());
            if (CommonUtil.isNotEmptyList(this.list(param))) {
                throw new BaseException(BaseErrorCode.NAME_IS_USED);
            }
        }
    }

    @SuppressWarnings("Duplicates")
    private Specification<Company> getSpecification(Company entity) {
        return (root, query, cb) -> {
            List<Predicate> list = new ArrayList<>();
            SpecificationUtil.baseSpecification(root, cb, list, entity);
            if (StringUtils.isNotBlank(entity.getName())) {
                Predicate predicate = cb.equal(root.get("name"), entity.getName().trim());
                list.add(predicate);
            }
            query.where(list.toArray(new Predicate[0]));
            return query.getRestriction();
        };
    }
}
