package com.chensi.admin.shop.dao;

import com.chensi.admin.shop.domain.GoodsPic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author si.chen
 * @date 2019/7/15 11:12
 */
@Repository
public interface GoodsPicRepository extends JpaRepository<GoodsPic, String>, JpaSpecificationExecutor<GoodsPic> {
}
