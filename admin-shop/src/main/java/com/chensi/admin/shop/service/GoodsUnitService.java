package com.chensi.admin.shop.service;

import com.chensi.admin.shop.domain.GoodsUnit;
import com.chensi.admin.shop.exception.BaseException;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/15 17:10
 */
public interface GoodsUnitService {
    /**
     * 新建
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void create(GoodsUnit entity) throws BaseException;

    /**
     * 查询多个
     *
     * @param entity 实体
     * @return 实体
     * @throws BaseException 基本异常
     */
    List<GoodsUnit> list(GoodsUnit entity) throws BaseException;

    /**
     * 修改
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void update(GoodsUnit entity) throws BaseException;

    /**
     * 删除
     *
     * @param id id
     * @throws BaseException 基本异常
     */
    void delete(String id) throws BaseException;

    /**
     * 查询单个
     *
     * @param id id
     * @return 实体
     * @throws BaseException 基本异常
     */
    GoodsUnit get(String id) throws BaseException;
}
