package com.chensi.admin.shop.controller;

import com.chensi.admin.shop.common.Constants;
import com.chensi.admin.shop.common.JsonResult;
import com.chensi.admin.shop.controller.api.CompanyItemApi;
import com.chensi.admin.shop.domain.CompanyItem;
import com.chensi.admin.shop.exception.BaseException;
import com.chensi.admin.shop.service.CompanyItemService;
import com.chensi.admin.shop.dto.CompanyItemDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/19 10:27
 */
@RestController
@RequestMapping("companyItem")
public class CompanyItemController implements CompanyItemApi {

    private final CompanyItemService companyItemService;

    @Autowired
    public CompanyItemController(CompanyItemService companyItemService) {
        this.companyItemService = companyItemService;
    }

    @Override
    @PostMapping("list")
    public ResponseEntity<JsonResult> list(@RequestBody CompanyItem companyItem) throws BaseException {
        List<CompanyItemDTO> list = companyItemService.list(companyItem);
        return ResponseEntity.ok(JsonResult.ok(list));
    }

    @Override
    @PostMapping("")
    public ResponseEntity<JsonResult> create(@Validated @RequestBody CompanyItem companyItem) throws BaseException {
        companyItemService.create(companyItem);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PutMapping("")
    public ResponseEntity<JsonResult> update(@Validated({Constants.CheckId.class})
                                             @RequestBody CompanyItem companyItem) throws BaseException {
        companyItemService.update(companyItem);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @DeleteMapping("{id}")
    public ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException {
        companyItemService.delete(id);
        return ResponseEntity.ok(JsonResult.ok());
    }
}
