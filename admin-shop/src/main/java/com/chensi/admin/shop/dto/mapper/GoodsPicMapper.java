package com.chensi.admin.shop.dto.mapper;

import com.chensi.admin.shop.domain.GoodsPic;
import com.chensi.admin.shop.dto.GoodsPicDTO;
import org.mapstruct.Mapper;

/**
 * @author si.chen
 * @date 2019/7/15 15:27
 */
@Mapper(componentModel = "spring")
public interface GoodsPicMapper extends BaseMapper<GoodsPic, GoodsPicDTO> {
}
