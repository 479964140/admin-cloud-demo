package com.chensi.admin.shop.util;

import com.chensi.admin.shop.domain.BaseEntity;
import org.apache.commons.lang.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/15 9:30
 */
public class SpecificationUtil {

    /**
     * 基本属性查询条件
     */
    public static <T extends BaseEntity> void baseSpecification(Root<T> root, CriteriaBuilder cb, List<Predicate> list,
                                                                T entity) {
        //ID不等于
        if (StringUtils.isNotBlank(entity.getIdNe())) {
            Predicate predicate = cb.notEqual(root.get("id"), entity.getIdNe().trim());
            list.add(predicate);
        }
        //name属性like % %
        if (StringUtils.isNotBlank(entity.getNameLike())) {
            Predicate predicate = cb.like(root.get("name"), "%" + entity.getNameLike().trim() + "%");
            list.add(predicate);
        }
    }
}
