package com.chensi.admin.shop.domain;

import com.chensi.admin.shop.common.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 商家
 *
 * @author si.chen
 * @date 2019/7/8 16:03
 */
@Setter
@Getter
@Entity(name = "company")
@Where(clause = "is_deleted = 0")
public class Company extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -5691750767767798750L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "商家名")
    @NotBlank(message = "商家名不为空")
    @Column(name = "name")
    private String name;

    @ApiModelProperty(value = "商家主题")
    @NotBlank(message = "商家主题不为空")
    @Column(name = "title")
    private String title;

    @ApiModelProperty(value = "商家图片")
    @Column(name = "pic")
    private String pic;

    @ApiModelProperty(value = "费用范围")
    @Column(name = "fee_scope")
    private String feeScope;

    @ApiModelProperty(value = "电话")
    @Column(name = "tel")
    @NotBlank(message = "电话不为空")
    private String tel;

    @ApiModelProperty(value = "营业时间")
    @Column(name = "business_hour")
    private String businessHour;

    @ApiModelProperty(value = "地址")
    @Column(name = "address")
    private String address;

    @ApiModelProperty(value = "经度")
    @Column(name = "px")
    private String px;

    @ApiModelProperty(value = "纬度")
    @Column(name = "py")
    private Integer py;

    @ApiModelProperty(value = "服务范围")
    @Column(name = "service_scope")
    private String serviceScope;

    @ApiModelProperty(value = "服务类型")
    @Column(name = "service_type")
    private String serviceType;

    @ApiModelProperty(value = "服务详情")
    @Column(name = "service_detail")
    private String serviceDetail;

    @Column(name = "is_deleted")
    private Integer deleted = Constants.NO;

    //=====================================

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
    private List<CompanyItem> companyServiceList;

}
