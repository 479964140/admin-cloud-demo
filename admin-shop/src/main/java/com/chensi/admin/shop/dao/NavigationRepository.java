package com.chensi.admin.shop.dao;

import com.chensi.admin.shop.domain.Navigation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author si.chen
 * @date 2019/7/19 9:05
 */
@Repository
public interface NavigationRepository extends JpaRepository<Navigation, String>, JpaSpecificationExecutor<Navigation> {
}
