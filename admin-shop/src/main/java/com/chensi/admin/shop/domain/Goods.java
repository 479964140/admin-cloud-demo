package com.chensi.admin.shop.domain;

import com.chensi.admin.shop.common.Constants;
import com.chensi.admin.shop.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 商品信息
 *
 * @author si.chen
 * @date 2019/7/8 16:13
 */
@Setter
@Getter
@Entity(name = "goods")
@Where(clause = "is_deleted = 0")
@EntityListeners(AuditingEntityListener.class)
public class Goods extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -8890936494171487619L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    @ApiModelProperty(value = "ID")
    private String id;

    @NotBlank(message = "商品名不为空")
    @ApiModelProperty(value = "商品名")
    @Column(name = "name")
    private String name;

    @ApiModelProperty(value = "缩略图")
    @Column(name = "thumb")
    private String thumb;

    @NotNull(message = "价格不为空")
    @ApiModelProperty(value = "价格")
    @Column(name = "price")
    private Float price;

    @ApiModelProperty(value = "价格最大")
    @Column(name = "price_max")
    private Float priceMax;

    @ApiModelProperty(value = "价格最小")
    @Column(name = "price_min")
    private Float priceMin;

    @NotNull(message = "单位不为空")
    @ApiModelProperty(value = "计量单位id")
    @JoinColumn(name = "unit")
    @ManyToOne(fetch = FetchType.LAZY)
    private GoodsUnit goodsUnit;

    @NotNull(message = "商品类型不为空")
    @ApiModelProperty(value = "商品类型id")
    @JoinColumn(name = "goods_type_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private GoodsType goodsType;

    @NotNull(message = "商家不为空")
    @ApiModelProperty(value = "商家id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;


    @ApiModelProperty(value = "是否推荐")
    @Column(name = "is_recommended")
    private Integer isRecommended;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @CreatedDate
    @Column(name = "create_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private Date createTime;

    @LastModifiedDate
    @Column(name = "update_time")
    @JsonFormat(pattern = DateUtil.DATE_FORMAT_DEFAULT)
    private Date updateTime;

    @Column(name = "is_deleted")
    private Integer deleted = Constants.NO;

    //=====================================

    @OneToMany(mappedBy = "goods", cascade = {CascadeType.ALL})
    private List<GoodsUser> goodsUserList;

    @OneToMany(mappedBy = "goods", cascade = {CascadeType.ALL})
    private List<GoodsPic> goodsPicList;

    @Transient
    private Float priceStart;

    @Transient
    private Float priceEnd;


}
