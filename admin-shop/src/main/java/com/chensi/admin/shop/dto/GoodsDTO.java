package com.chensi.admin.shop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/9 15:06
 */
@Data
public class GoodsDTO implements Serializable {

    private static final long serialVersionUID = -1184946454944124631L;

    @ApiModelProperty(value = "ID")
    private String id;

    @ApiModelProperty(value = "商品名")
    private String name;

    @ApiModelProperty(value = "缩略图")
    private String thumb;

    @ApiModelProperty(value = "价格")
    private Float price;

    @ApiModelProperty(value = "价格最大")
    private Float priceMax;

    @ApiModelProperty(value = "价格最小")
    private Float priceMin;

    @ApiModelProperty(value = "unitId")
    private String unitId;

    @ApiModelProperty(value = "unitName")
    private String unitName;

    @ApiModelProperty(value = "goodsTypeId")
    private String goodsTypeId;

    @ApiModelProperty(value = "goodsTypeName")
    private String goodsTypeName;

    @ApiModelProperty(value = "companyId")
    private String companyId;

    @ApiModelProperty(value = "companyName")
    private String companyName;

    @ApiModelProperty(value = "是否推荐")
    private Integer isRecommended;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    private List<GoodsPicDTO> goodsPicList;

}
