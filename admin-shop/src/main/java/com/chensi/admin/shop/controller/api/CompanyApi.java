package com.chensi.admin.shop.controller.api;

import com.chensi.admin.shop.common.Constants;
import com.chensi.admin.shop.common.JsonResult;
import com.chensi.admin.shop.domain.Company;
import com.chensi.admin.shop.exception.BaseException;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author si.chen
 * @date 2019/7/19 10:26
 */
@Api(tags = "商家接口文档")
public interface CompanyApi {

    /**
     * 详情
     *
     * @param id id
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "详情", notes = "入参:id<br>验证规则：id不可为空", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "path",
                    dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "items: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @GetMapping("{id}")
    ResponseEntity<JsonResult> get(@PathVariable("id") String id) throws BaseException;

    /**
     * 新增
     *
     * @param company company
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "新增", notes = "入参：name,title,pic,freeScope,tel,businessHour,address,px,py,serviceScope," +
            "serviceType,serviceDetail" +
            "<br> 验证规则：name,title,tel非空",
            response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "items: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PostMapping("")
    ResponseEntity<JsonResult> create(@Validated @RequestBody Company company) throws BaseException;

    /**
     * 修改
     *
     * @param company company
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "修改", notes = "入参：id,name,title,pic,freeScope,tel,businessHour,address,px,py,serviceScope" +
            "serviceType,serviceDetail" +
            "<br> 验证规则：id不为空",
            response = JsonResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "items: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @PutMapping("")
    ResponseEntity<JsonResult> update(@Validated({Constants.CheckId.class})
                                      @RequestBody Company company) throws BaseException;

    /**
     * 删除
     *
     * @param id id
     * @return 消息体
     * @throws BaseException 基本异常
     */
    @ApiOperation(value = "删除", notes = "入参:id <br> 验证规则：id不可为空", response = JsonResult.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "path",
                    dataType = "String")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success", response = JsonResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = JsonResult.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = JsonResult.class),
            @ApiResponse(code = 404, message = "items: id is not found", response = JsonResult.class),
            @ApiResponse(code = 500, message = "Server Internal Error", response = JsonResult.class)})
    @DeleteMapping("{id}")
    ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException;
}
