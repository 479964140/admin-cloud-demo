package com.chensi.admin.shop.domain;

import com.chensi.admin.shop.common.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 导航图
 *
 * @author si.chen
 * @date 2019/7/8 15:27
 */
@Setter
@Getter
@Entity(name = "navigation")
public class Navigation extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -7075677706669173390L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    @ApiModelProperty(value = "ID")
    private String id;

    @ApiModelProperty(value = "导航图片")
    @Column(name = "pic")
    private String pic;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;
}
