package com.chensi.admin.shop.domain;

import com.chensi.admin.shop.common.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 设备类型
 *
 * @author si.chen
 * @date 2019/7/9 10:34
 */
@Setter
@Getter
@Entity(name = "device_type")
@Where(clause = "is_deleted = 0")
public class DeviceType extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -3642011771791311887L;
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "设备类型")
    @Column(name = "name")
    private Integer name;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @Column(name = "is_deleted")
    private Integer deleted = Constants.NO;
}
