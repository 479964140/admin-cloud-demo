package com.chensi.admin.shop.service;

import com.chensi.admin.shop.domain.Navigation;
import com.chensi.admin.shop.exception.BaseException;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/19 9:06
 */
public interface NavigationService {

    /**
     * 新建
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void create(Navigation entity) throws BaseException;

    /**
     * 查询多个
     *
     * @return 实体
     * @throws BaseException 基本异常
     */
    List<Navigation> list() throws BaseException;

    /**
     * 修改
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void update(Navigation entity) throws BaseException;

    /**
     * 删除
     *
     * @param id id
     * @throws BaseException 基本异常
     */
    void delete(String id) throws BaseException;

    /**
     * 查询单个
     *
     * @param id id
     * @return 实体
     * @throws BaseException 基本异常
     */
    Navigation get(String id) throws BaseException;
}
