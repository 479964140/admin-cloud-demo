package com.chensi.admin.shop.domain;

import com.chensi.admin.shop.common.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 商品--单位
 *
 * @author si.chen
 * @date 2019/7/8 16:03
 */
@Setter
@Getter
@Entity(name = "goods_unit")
public class GoodsUnit extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -8742582981409332804L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "单位")
    @Column(name = "name")
    private String name;

}
