package com.chensi.admin.shop.domain;

import com.chensi.admin.shop.common.Constants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 用户--商品
 *
 * @author si.chen
 * @date 2019/7/8 16:03
 */
@Setter
@Getter
@Entity(name = "goods_user")
public class GoodsUser extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 7338432089318324548L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @NotBlank(message = "ID不为空", groups = {Constants.CheckId.class})
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "用户id")
    @Column(name = "user_id")
    @NotBlank(message = "userId不为空")
    private String userId;

    @ApiModelProperty(value = "商品id")
    @NotNull(message = "商品id不为空")
    @JoinColumn(name = "goods_id")
    @ManyToOne
    private Goods goods;
}
