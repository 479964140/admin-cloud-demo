package com.chensi.admin.shop.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author si.chen
 * @date 2019/7/16 9:44
 */
@Data
public class GoodsUserDTO implements Serializable {

    private static final long serialVersionUID = 4403697354346551589L;

    private String id;

    private String userId;

    private String goodsId;

    private String goodsName;

}
