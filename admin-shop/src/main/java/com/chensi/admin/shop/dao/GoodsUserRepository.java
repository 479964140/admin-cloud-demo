package com.chensi.admin.shop.dao;

import com.chensi.admin.shop.domain.GoodsUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author si.chen
 * @date 2019/7/15 17:36
 */
@Repository
public interface GoodsUserRepository extends JpaRepository<GoodsUser, String>, JpaSpecificationExecutor<GoodsUser> {
}
