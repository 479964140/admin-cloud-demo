package com.chensi.admin.shop.dto.mapper;

import com.chensi.admin.shop.domain.GoodsUser;
import com.chensi.admin.shop.dto.GoodsUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author si.chen
 * @date 2019/7/16 9:47
 */
@Mapper(componentModel = "spring")
public interface GoodsUserMapper extends BaseMapper<GoodsUser, GoodsUserDTO> {

    @Override
    @Mappings({
            @Mapping(source = "goods.id", target = "goodsId"),
            @Mapping(source = "goods.name", target = "goodsName"),
    })
    GoodsUserDTO toDTO(GoodsUser goodsUser);
}
