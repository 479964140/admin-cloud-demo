package com.chensi.admin.shop.controller;

import com.chensi.admin.shop.common.Constants;
import com.chensi.admin.shop.common.JsonResult;
import com.chensi.admin.shop.domain.Goods;
import com.chensi.admin.shop.dto.GoodsDTO;
import com.chensi.admin.shop.service.GoodsService;
import com.chensi.admin.shop.controller.api.GoodsApi;
import com.chensi.admin.shop.exception.BaseException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/9 9:33
 */
@Log4j2
@RestController
@RequestMapping("goods")
@AllArgsConstructor
public class GoodsController implements GoodsApi {

    private final GoodsService goodsService;

    @Override
    @GetMapping("{id}")
    public ResponseEntity<JsonResult> get(@PathVariable("id") String id) throws BaseException {
        GoodsDTO dto = goodsService.detail(id);
        return ResponseEntity.ok(JsonResult.ok(dto));
    }

    @Override
    @PostMapping("find")
    public ResponseEntity<JsonResult> find(@RequestBody Goods goods) throws BaseException {
        return ResponseEntity.ok(JsonResult.ok(goodsService.find(goods)));
    }

    @Override
    @PostMapping("list")
    public ResponseEntity<JsonResult> list(@RequestBody Goods goods) throws BaseException {
        List list = goodsService.list(goods);
        return ResponseEntity.ok(JsonResult.ok(list));
    }

    @Override
    @PostMapping("page")
    public ResponseEntity<JsonResult> page(@RequestBody Goods goods) {
        return ResponseEntity.ok(JsonResult.ok(goodsService.page(goods)));
    }

    @Override
    @PostMapping("")
    public ResponseEntity<JsonResult> create(@Validated @RequestBody Goods goods)
            throws BaseException {
        goodsService.create(goods);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PutMapping("")
    public ResponseEntity<JsonResult> update(@Validated({Constants.CheckId.class})
                                             @RequestBody Goods goods) throws BaseException {
        goodsService.update(goods);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @DeleteMapping("{id}")
    public ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException {
        goodsService.delete(id);
        return ResponseEntity.ok(JsonResult.ok());
    }
}
