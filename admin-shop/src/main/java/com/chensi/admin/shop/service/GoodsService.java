package com.chensi.admin.shop.service;

import com.chensi.admin.shop.domain.Goods;
import com.chensi.admin.shop.dto.GoodsDTO;
import com.chensi.admin.shop.exception.BaseException;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/9 9:30
 */
public interface GoodsService {

    /**
     * 查询单个
     *
     * @param id id
     * @return 商品信息
     * @throws BaseException 基本异常
     */
    Goods get(String id) throws BaseException;

    /**
     * 查询单个
     *
     * @param id id
     * @return 商品信息
     * @throws BaseException 基本异常
     */
    GoodsDTO detail(String id) throws BaseException;

    /**
     * 查询单个
     *
     * @param goods 商品信息
     * @return 商品信息
     * @throws BaseException 基本异常
     */
    GoodsDTO find(Goods goods) throws BaseException;

    /**
     * 查询多个
     *
     * @param goods 商品信息
     * @return 商品信息
     * @throws BaseException 基本异常
     */
    List<GoodsDTO> list(Goods goods) throws BaseException;

    /**
     * 查询分页
     *
     * @param goods 商品信息
     * @return 商品信息
     */
    Page<GoodsDTO> page(Goods goods);

    /**
     * 新建
     *
     * @param goods 商品
     * @throws BaseException 基本异常
     */
    void create(Goods goods) throws BaseException;

    /**
     * 修改
     *
     * @param goods 商品
     * @throws BaseException 基本异常
     */
    void update(Goods goods) throws BaseException;

    /**
     * 删除
     *
     * @param id id
     * @throws BaseException 基本异常
     */
    void delete(String id) throws BaseException;

}
