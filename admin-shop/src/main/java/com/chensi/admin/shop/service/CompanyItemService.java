package com.chensi.admin.shop.service;

import com.chensi.admin.shop.domain.CompanyItem;
import com.chensi.admin.shop.dto.CompanyItemDTO;
import com.chensi.admin.shop.exception.BaseException;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/19 8:58
 */
public interface CompanyItemService {

    /**
     * 查询多个
     *
     * @param entity 实体
     * @return 实体列表
     * @throws BaseException 基本异常
     */
    List<CompanyItemDTO> list(CompanyItem entity) throws BaseException;

    /**
     * 查询单个
     *
     * @param id id
     * @return 实体
     * @throws BaseException 基本异常
     */
    CompanyItem get(String id) throws BaseException;

    /**
     * 新建
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void create(CompanyItem entity) throws BaseException;

    /**
     * 修改
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void update(CompanyItem entity) throws BaseException;

    /**
     * 删除
     *
     * @param id id
     * @throws BaseException 基本异常
     */
    void delete(String id) throws BaseException;
}
