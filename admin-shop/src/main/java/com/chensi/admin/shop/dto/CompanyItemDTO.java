package com.chensi.admin.shop.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author si.chen
 * @date 2019/7/22 16:04
 */
@Data
public class CompanyItemDTO implements Serializable {
    private static final long serialVersionUID = -5099287693536186672L;

    private String id;

    private String companyId;

    private String companyName;

    private String name;

    private String detail;
}
