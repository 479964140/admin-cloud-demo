package com.chensi.admin.shop.service;

import com.chensi.admin.shop.domain.GoodsUser;
import com.chensi.admin.shop.dto.GoodsUserDTO;
import com.chensi.admin.shop.exception.BaseException;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/16 9:42
 */
public interface GoodsUserService {
    /**
     * 查询多个
     *
     * @param entity 实体
     * @return 实体DTO
     * @throws BaseException 基本异常
     */
    List<GoodsUserDTO> list(GoodsUser entity) throws BaseException;

    /**
     * 查询分页
     *
     * @param entity 实体
     * @return 实体DTO
     */
    Page<GoodsUserDTO> page(GoodsUser entity);

    /**
     * 新建
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void create(GoodsUser entity) throws BaseException;

    /**
     * 删除
     *
     * @param id id
     * @throws BaseException 基本异常
     */
    void delete(String id) throws BaseException;
}
