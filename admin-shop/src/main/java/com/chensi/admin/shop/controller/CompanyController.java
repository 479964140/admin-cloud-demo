package com.chensi.admin.shop.controller;

import com.chensi.admin.shop.common.Constants;
import com.chensi.admin.shop.common.JsonResult;
import com.chensi.admin.shop.domain.Company;
import com.chensi.admin.shop.service.CompanyService;
import com.chensi.admin.shop.controller.api.CompanyApi;
import com.chensi.admin.shop.exception.BaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author si.chen
 * @date 2019/7/19 10:27
 */
@RestController
@RequestMapping("company")
public class CompanyController implements CompanyApi {

    private final CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @Override
    @GetMapping("{id}")
    public ResponseEntity<JsonResult> get(@PathVariable("id") String id) throws BaseException {
        Company company = companyService.get(id);
        return ResponseEntity.ok(JsonResult.ok(company));
    }

    @Override
    @PostMapping("")
    public ResponseEntity<JsonResult> create(@Validated @RequestBody Company company) throws BaseException {
        companyService.create(company);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PutMapping("")
    public ResponseEntity<JsonResult> update(@Validated({Constants.CheckId.class})
                                             @RequestBody Company company) throws BaseException {
        companyService.update(company);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @DeleteMapping("{id}")
    public ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException {
        companyService.delete(id);
        return ResponseEntity.ok(JsonResult.ok());
    }
}
