package com.chensi.admin.shop.service;

import com.chensi.admin.shop.domain.Company;
import com.chensi.admin.shop.exception.BaseException;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/19 8:58
 */
public interface CompanyService {

    /**
     * 查询单个
     *
     * @param id id
     * @return 实体
     * @throws BaseException 基本异常
     */
    Company get(String id) throws BaseException;

    /**
     * 查询列表
     *
     * @param entity 实体
     * @return 实体
     * @throws BaseException 基本异常
     */
    List<Company> list(Company entity) throws BaseException;

    /**
     * 新建
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void create(Company entity) throws BaseException;

    /**
     * 修改
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void update(Company entity) throws BaseException;

    /**
     * 删除
     *
     * @param id id
     * @throws BaseException 基本异常
     */
    void delete(String id) throws BaseException;
}
