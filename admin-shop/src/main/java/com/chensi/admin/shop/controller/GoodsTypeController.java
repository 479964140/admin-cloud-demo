package com.chensi.admin.shop.controller;

import com.chensi.admin.shop.common.Constants;
import com.chensi.admin.shop.common.JsonResult;
import com.chensi.admin.shop.domain.GoodsType;
import com.chensi.admin.shop.service.GoodsTypeService;
import com.chensi.admin.shop.controller.api.GoodsTypeApi;
import com.chensi.admin.shop.exception.BaseException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author si.chen
 * @date 2019/7/9 9:33
 */
@Log4j2
@RestController
@RequestMapping("goodsType")
public class GoodsTypeController implements GoodsTypeApi {

    private final GoodsTypeService goodsTypeService;

    @Autowired
    public GoodsTypeController(GoodsTypeService goodsTypeService) {
        this.goodsTypeService = goodsTypeService;
    }

    @Override
    @PostMapping("list")
    public ResponseEntity<JsonResult> list(@RequestBody GoodsType goodsType) throws BaseException {
        return ResponseEntity.ok(JsonResult.ok(goodsTypeService.list(goodsType)));
    }

    @Override
    @PostMapping("")
    public ResponseEntity<JsonResult> create(@Validated @RequestBody GoodsType goodsType) throws BaseException {
        goodsTypeService.create(goodsType);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @PutMapping("")
    public ResponseEntity<JsonResult> update(@Validated({Constants.CheckId.class}) @RequestBody GoodsType goodsType)
            throws BaseException {
        goodsTypeService.update(goodsType);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @DeleteMapping("{id}")
    public ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException {
        goodsTypeService.delete(id);
        return ResponseEntity.ok(JsonResult.ok());
    }
}
