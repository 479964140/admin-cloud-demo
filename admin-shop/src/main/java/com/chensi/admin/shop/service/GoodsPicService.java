package com.chensi.admin.shop.service;

import com.chensi.admin.shop.domain.GoodsPic;
import com.chensi.admin.shop.exception.BaseException;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/15 13:20
 */
public interface GoodsPicService {

    /**
     * 查询多个
     *
     * @param entity 商品图片
     * @return 商品图片
     * @throws BaseException 基本异常
     */
    List<GoodsPic> list(GoodsPic entity) throws BaseException;
}
