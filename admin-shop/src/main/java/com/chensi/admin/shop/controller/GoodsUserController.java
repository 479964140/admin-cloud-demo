package com.chensi.admin.shop.controller;

import com.chensi.admin.shop.common.JsonResult;
import com.chensi.admin.shop.domain.GoodsUser;
import com.chensi.admin.shop.dto.GoodsUserDTO;
import com.chensi.admin.shop.service.GoodsUserService;
import com.chensi.admin.shop.controller.api.GoodsUserApi;
import com.chensi.admin.shop.exception.BaseException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/9 9:33
 */
@Log4j2
@RestController
@RequestMapping("goodsUser")
public class GoodsUserController implements GoodsUserApi {

    private final GoodsUserService goodsUserService;

    @Autowired
    public GoodsUserController(GoodsUserService goodsUserService) {
        this.goodsUserService = goodsUserService;
    }

    @Override
    @PostMapping("list")
    public ResponseEntity<JsonResult> list(@RequestBody GoodsUser goodsUser) throws BaseException {
        List<GoodsUserDTO> list = goodsUserService.list(goodsUser);
        return ResponseEntity.ok(JsonResult.ok(list));

    }

    @Override
    @PostMapping("page")
    public ResponseEntity<JsonResult> page(@RequestBody GoodsUser goodsUser) {
        Page<GoodsUserDTO> page = goodsUserService.page(goodsUser);
        return ResponseEntity.ok(JsonResult.ok(page));
    }

    @Override
    @PostMapping("")
    public ResponseEntity<JsonResult> create(@Validated @RequestBody GoodsUser goodsUser) throws BaseException {
        goodsUserService.create(goodsUser);
        return ResponseEntity.ok(JsonResult.ok());
    }

    @Override
    @DeleteMapping("{id}")
    public ResponseEntity<JsonResult> delete(@PathVariable("id") String id) throws BaseException {
        goodsUserService.delete(id);
        return ResponseEntity.ok(JsonResult.ok());
    }
}
