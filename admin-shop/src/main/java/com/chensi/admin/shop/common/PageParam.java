package com.chensi.admin.shop.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author si.chen
 * @date 2019/7/10 16:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageParam {

    private Integer pageNo = Constants.PAGE_NUM;

    private Integer pageSize = Constants.PAGE_SIZE;

}
