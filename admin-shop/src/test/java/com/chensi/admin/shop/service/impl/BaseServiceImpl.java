package com.chensi.admin.shop.service.impl;

import com.chensi.admin.shop.common.BaseErrorCode;
import com.chensi.admin.shop.exception.BaseException;
import com.chensi.admin.shop.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author si.chen
 * @date 2019/7/15 13:55
 */
@Service
public class BaseServiceImpl<T, D> implements BaseService<T, D> {

    @Autowired
    private JpaRepository<T, String> jpaRepository;

    @Override
    public T get(String id) throws BaseException {
        Optional<T> optional = jpaRepository.findById(id);
        return optional.orElseThrow(() -> new BaseException(BaseErrorCode.ITEM_NOT_FOUND));
    }

    @Override
    public D detail(String id) throws BaseException {
        return null;
    }

    @Override
    public D find(T entity) throws BaseException {
        return null;
    }

    @Override
    public List<D> list(T entity) throws BaseException {
        return null;
    }

    @Override
    public Page<D> page(T entity) {
        return null;
    }

    @Override
    public void create(T entity) throws BaseException {

    }

    @Override
    public void update(T entity) throws BaseException {

    }

    @Override
    public void delete(String id) throws BaseException {

    }
}
