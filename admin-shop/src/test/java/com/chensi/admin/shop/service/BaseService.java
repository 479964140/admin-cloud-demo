package com.chensi.admin.shop.service;

import com.chensi.admin.shop.exception.BaseException;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/15 13:46
 */
public interface BaseService<T, D> {

    /**
     * 查询单个
     *
     * @param id id
     * @return 实体
     * @throws BaseException 基本异常
     */
    T get(String id) throws BaseException;

    /**
     * 查询单个
     *
     * @param id id
     * @return 实体DTO
     * @throws BaseException 基本异常
     */
    D detail(String id) throws BaseException;

    /**
     * 查询单个
     *
     * @param entity 实体
     * @return 实体DTO
     * @throws BaseException 基本异常
     */
    D find(T entity) throws BaseException;

    /**
     * 查询多个
     *
     * @param entity 实体
     * @return 实体DTO
     * @throws BaseException 基本异常
     */
    List<D> list(T entity) throws BaseException;

    /**
     * 查询分页
     *
     * @param entity 实体
     * @return 实体DTO
     */
    Page<D> page(T entity);

    /**
     * 新建
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void create(T entity) throws BaseException;

    /**
     * 修改
     *
     * @param entity 实体
     * @throws BaseException 基本异常
     */
    void update(T entity) throws BaseException;

    /**
     * 删除
     *
     * @param id id
     * @throws BaseException 基本异常
     */
    void delete(String id) throws BaseException;
}
