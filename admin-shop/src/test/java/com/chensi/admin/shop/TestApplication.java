package com.chensi.admin.shop;

import com.chensi.admin.shop.domain.*;
import com.chensi.admin.shop.exception.BaseException;
import com.chensi.admin.shop.service.GoodsService;
import com.chensi.admin.shop.service.GoodsTypeService;
import com.chensi.admin.shop.service.GoodsUnitService;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author si.chen
 * @date 2019/7/8 15:01
 */
@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestApplication {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private GoodsTypeService goodsTypeService;

    @Autowired
    private GoodsUnitService goodsUnitService;

    @Test
    public void test() throws BaseException {
        Goods param = new Goods();
        param.setName("空气");
        GoodsType type = new GoodsType();
        type.setId("2");
        param.setGoodsType(type);
        System.out.println(goodsService.find(param));
    }

    @Test
    public void test2() throws BaseException {
        System.out.println(goodsService.list(new Goods()));
    }

    @Test
    public void test3() {
        System.out.println(goodsService.page(new Goods()));
    }

    @Test
    public void test4() throws BaseException {
        System.out.println(goodsService.get("1"));
    }

    @Test
    public void test5() throws BaseException {
        System.out.println(goodsService.detail("1"));
    }

    @Test
    public void test6() throws BaseException {
        Goods goods = new Goods();
        goods.setName("维修2");
        goods.setPrice(200.5f);
        GoodsType type = new GoodsType();
        type.setId("1");
        goods.setGoodsType(type);
        GoodsUnit unit = new GoodsUnit();
        unit.setId("1");
        goods.setGoodsUnit(unit);
        Company company = new Company();
        company.setId("1");
        goods.setCompany(company);
        List<GoodsPic> list = new ArrayList<>();
        GoodsPic pic = new GoodsPic();
        pic.setPic("xxx");
        list.add(pic);
        goods.setGoodsPicList(list);
        goodsService.create(goods);
    }

    @Test
    public void test7() throws BaseException {
        Goods goods = new Goods();
        goods.setId("1");
        goods.setName("空气净化1");
        goods.setPrice(200.5f);
        GoodsType type = new GoodsType();
        type.setId("2");
        goods.setGoodsType(type);
        GoodsUnit unit = new GoodsUnit();
        unit.setId("2");
        goods.setGoodsUnit(unit);
        Company company = new Company();
        company.setId("2");
        goods.setCompany(company);
        goodsService.update(goods);
    }

    @Test
    public void test8() throws BaseException {
        goodsService.delete("1");
    }

    @Test
    public void test9() throws BaseException {
        GoodsType param = new GoodsType();
        param.setName("农产品");
        param.setSort(4);
        param.setLevel(1);
        goodsTypeService.create(param);
    }

    @Test
    public void test10() throws BaseException {
        GoodsType param = new GoodsType();
        param.setId("1");
        param.setName("农产品1");
        param.setSort(4);
        param.setLevel(1);
        goodsTypeService.update(param);
    }

    @Test
    public void test11() throws BaseException {
        GoodsType param = new GoodsType();
        param.setNameLike("维修");
        System.out.println(goodsTypeService.list(param));
    }

    @Test
    public void test12() throws BaseException {
        goodsTypeService.delete("4028df816be3ef05016be3ef507b0000");
    }

    @Test
    public void test13() throws BaseException {
        Goods param = new Goods();
        param.setNameLike("维修2");
        System.out.println(goodsService.list(param));
    }

    @Test
    public void test14() throws BaseException {
        GoodsUnit goodsUnit = new GoodsUnit();
        goodsUnit.setName("元/平方米");
        goodsUnitService.create(goodsUnit);
    }

    @Test
    public void test15() throws BaseException {
        GoodsUnit unit = new GoodsUnit();
        System.out.println(goodsUnitService.list(unit));
    }

}
