# admin-cloud-demo

#### Description
springcloud demo项目，仅做demo使用，不做迭代开发，后期会创建新的admin-cloud项目做迭代开发。技术栈：springcloud2.x，springboot2.x，feign接口调用，hystrix熔断级，swagger2接口文档，jdk8，JPA持久化，redis缓存，mapStruct对象转换映射，postgresql数据库，flyway数据库版本控制，shiro权限控制

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)